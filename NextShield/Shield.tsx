import { ReactNode } from "react";
import { useRouter } from "next/router";
import { NextShield, NextShieldProps } from "next-shield";
import Loading from "../Components/Loading";

export interface Children {
  children: ReactNode;
}

export function Shield({ children }: Children) {
  const router = useRouter();

  const token =
    typeof window !== "undefined" ? window.localStorage.getItem("token") : null;

  const shieldProps: NextShieldProps<["/mi-cuenta"], ["/login"]> = {
    router,
    isAuth: token ? true : false,
    isLoading: token ? false : false,
    privateRoutes: ["/mi-cuenta"],
    publicRoutes: ["/login"],
    hybridRoutes: ["/login"],
    loginRoute: "/login",
    accessRoute: "/mi-cuenta",
    LoadingComponent: <Loading />,
  };

  return <NextShield {...shieldProps}>{children}</NextShield>;
}
