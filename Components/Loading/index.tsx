import React from "react";
import { LoadingOutlined } from "@ant-design/icons";
import { Spin } from "antd";
import styles from "./index.module.css";

export default function Loading() {
  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;
  return (
    <div className={styles.LoadingCont}>
      <Spin indicator={antIcon} />
      <p style={{ marginLeft: 10 }} className={styles.loading}>
        Cargando...
      </p>
    </div>
  );
}
