import { message } from "antd";
import { MAIN_URL } from "../../utils/Urls";
export const onReset = async (email, setLoading, setIsModalVisible) => {
  setLoading(true);
  let res = await fetch(`${MAIN_URL}/forgotpassword?email=${email}`);
  if (res) {
    const user = await res.json();
    if (!user.success) {
      setLoading(false);
      message.warning("Aún no tenemos este email en la Federación");
    } else {
      setLoading(false);
      message.success("Email enviado con éxito");
      setIsModalVisible(false);
    }
  } else {
    message.error("Aún no tenemos este email en Federación");
    setLoading(false);
  }
};

export const onCreateUser = async (email) => {
  let res = await fetch(`${MAIN_URL}/forgotpassword?email=${email}`);
  if (res) {
    const user = await res.json();
  } else {
    message.error("Aún no tenemos este email en Federación");
  }
};
