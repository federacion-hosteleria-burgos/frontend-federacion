import React, { useState } from "react";
import styles from "./index.module.css";
import Form from "./Form";
import Footer from "../Footer";
import NavBar from "../NavBar";
import Forgot from "./Forgot";
import UnirmeForm from "../UnirmeForm"

export default function Login() {
  const [visible, setVisible] = useState(false);
  const [visibleUnir, setVisibleUnir] = useState(false);
  return (
    <div>
      <NavBar home={false} />
      <div className={styles.login_container}>
        <div className={styles.login_container_form} style={{ marginTop: 150 }}>
          <h2>Bienvenidos</h2>
          <Form onClick={() => setVisible(true)} onClickUnir={()=> setVisibleUnir(true)} />
        </div>
      </div>
      <Forgot isModalVisible={visible} setIsModalVisible={setVisible} />
      <UnirmeForm isModalVisible={visibleUnir} setIsModalVisible={setVisibleUnir} />
      <Footer />
    </div>
  );
}
