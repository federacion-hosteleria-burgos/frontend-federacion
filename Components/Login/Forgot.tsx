import { Modal, Form, Input, Button } from "antd";
import { MailOutlined } from "@ant-design/icons";
import { useState } from "react";
import { onReset } from "./funtion";

const Forgot = (props: any) => {
  const [Loading, setLoading] = useState(false);
  const { isModalVisible, setIsModalVisible } = props;

  const onFinish = (values: any) => {
    onReset(values.email, setLoading, setIsModalVisible);
  };

  return (
    <Modal
      title="Recuperar contraseña"
      visible={isModalVisible}
      footer={null}
      onCancel={() => setIsModalVisible(false)}
    >
      <div style={{ textAlign: "center", marginBottom: 30 }}>
        <h3>Recupera tu contraseña</h3>
        <p>
          Te enviaremos un enlace al Email con el que te has registrado en en la
          web de la federación para que reestablesca tu contraseña.
        </p>
      </div>

      <Form
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your email!",
              type: "email",
            },
          ]}
        >
          <Input
            prefix={<MailOutlined className="site-form-item-icon" />}
            placeholder="Correo electrónico"
            type="email"
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            loading={Loading}
            style={{ width: "100%", marginTop: 15 }}
          >
            Enviar enlace
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default Forgot;
