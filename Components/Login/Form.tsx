import React, { useState } from "react";
import { Form, Input, Button, Checkbox, message } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/client";
import { newMutation } from "../../GraphQL";
import { useRouter } from "next/router";

export default function Forms({ onClick, onClickUnir }) {
  const router = useRouter();
  const [Loading, setLoading] = useState(false);
  const [LoginUser] = useMutation(newMutation.AUTENTICAR_USUARIO);

  const onFinish = (values: any) => {
    setLoading(true);
    LoginUser({
      variables: { email: values.email, password: values.password },
    })
      .then((res: any) => {
        if (res.data.LoginUser.success) {
          setLoading(false);
          window.localStorage.setItem("id", res.data.LoginUser.data.id);
          window.localStorage.setItem("token", res.data.LoginUser.data.token);
          message.success("Bienvenido a la Federación de Hostelería de Burgos");
          setTimeout(() => {
            router.push("/mi-cuenta");
          }, 1500);
        } else {
          message.error(res.data.LoginUser.message);
          setLoading(false);
        }
      })
      .catch((error: any) => {
        setLoading(false);
        console.log(error);
        message.error("Algo salio mal intentalo de nuevo");
      });
  };

  return (
    <>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            { required: true, message: "Introduce un correo electrénico!" },
          ]}
        >
          <Input
            prefix={<MailOutlined className="site-form-item-icon" />}
            placeholder="Correo electrénico"
            style={{ height: 45 }}
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            { required: true, message: "Por favor introduce una contraseña!" },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Contraseña"
            style={{ height: 45 }}
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Recordar</Checkbox>
          </Form.Item>

          <a className="login-form-forgot" onClick={onClick}>
            ¿Se te olvidó?
          </a>
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
            loading={Loading}
            style={{ width: "100%", height: 45, borderRadius: 100 }}
          >
            Iniciar sesión
          </Button>
        </Form.Item>
        <Form.Item>
          O <a onClick={onClickUnir}>Únirme!</a>
        </Form.Item>
      </Form>
      <style jsx>
        {`
          .login-form {
            max-width: 300px;
          }
          .login-form-forgot {
            float: right;
          }
          .ant-col-rtl .login-form-forgot {
            float: left;
          }
          .login-form-button {
            width: 100%;
          }
        `}
      </style>
    </>
  );
}
