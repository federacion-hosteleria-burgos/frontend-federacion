import React from "react";
import styles from "./card.module.css";
import Link from "next/link";
import moment from "moment";
import "moment/locale/es";

export default function CardNew({ data, feed }) {
  return (
    <div
      className={
        feed ? styles.card_news_vertical_feed : styles.card_news_vertical
      }
    >
      <div
        className={
          feed
            ? styles.card_news_vertical_feed_img
            : styles.card_news_vertical_img
        }
      >
        <img src={data.image} alt={data.title} />
      </div>
      <div className={styles.card_news_vertical_info}>
        <h2>{data.title}</h2>
        <span>{moment(data.created_at).fromNow()}</span>
        <p>{data.description}</p>
        <Link href={data.link}>
          <a target="_black">Leer más</a>
        </Link>
      </div>
    </div>
  );
}
