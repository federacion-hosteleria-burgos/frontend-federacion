import React from "react";
import styles from "./index.module.css";
import Footer from "../Footer";
import NavBar from "../NavBar";
import Card from "../CardNew/card";
import { newQuery } from "../../GraphQL";
import { useQuery } from "@apollo/client";
import Loading from "../Loading";
import Ads from "../Home/Ads";

export default function Turismo() {
  const renderItem = (item, i) => {
    return <Card data={item} feed={true} key={i} />;
  };

  const {
    data = {},
    loading,
    refetch,
  } = useQuery(newQuery.GET_POST_PARAM, {
    variables: { input: { event: true }, page: 1, limit: 100 },
  });

  const { getPostParam } = data;

  const posts = getPostParam && getPostParam.data ? getPostParam.data : [];

  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      <NavBar home={false} />
      <div className={styles.feed_container}>
        <Ads />
        <div className={styles.feed_container_new}>
          <h1>Enventos y turismo</h1>
          <div className={styles.feed_container_new_content}>
            {posts &&
              posts.map((item, i) => {
                return renderItem(item, i);
              })}
          </div>
          <div
            style={{
              marginTop: 50,
              marginLeft: 70,
            }}
          >
            <button>Ver más</button>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
