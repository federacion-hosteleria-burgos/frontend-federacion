import styles from "../index.module.css";
import Footer from "../../Footer";
import NavBar from "../../NavBar";

export default function Privacity() {
  return (
    <div>
      <NavBar home={false} />
      <div className={styles.containers_hepls}>
        <div className={styles.cookies}>
        <div dangerouslySetInnerHTML={{ __html: `<p><strong>Identidad del Responsable del tratamiento de sus datos personales</strong></p>
<p>Federaci&oacute;n de Hosteler&iacute;a de Burgos, &quot;en adelante Federaci&oacute;n&quot; es el Responsable del tratamiento de</p>
<p>los datos personales del Usuario y le informa de que estos datos ser&aacute;n tratados de conformidad con lo</p>
<p>dispuesto en el Reglamento (UE) 2016/679, de 27 de abril (GDPR), y la Ley Org&aacute;nica 3/2018, de 5 de</p>
<p>diciembre (LOPDGDD).</p>
<p>Finalidades del Tratamiento</p>
<p>Las operaciones previstas para realizar el tratamiento son:</p>
<p>Tramitar encargos, solicitudes, dar respuesta a las consultas o cualquier tipo de petici&oacute;n que sea</p>
<p>realizada por el Usuario.</p>
<p>Cumplimentar nuestro formulario para solicita unirse a la Federaci&oacute;n.</p>
<p>La remisi&oacute;n de comunicaciones comerciales, newsletter o boletines online que sean de inter&eacute;s</p>
<p>del Usuario con su previo consentimiento.</p>
<p>La gesti&oacute;n del &aacute;rea privada del Asociado.</p>
<p>El an&aacute;lisis de la navegaci&oacute;n de los usuarios. La Federaci&oacute;n recoge otros datos no identificativos</p>
<p>que se obtienen mediante el uso de cookies que se descargan en el ordenador del Usuario</p>
<p>cuando navega por el sitio Web cuyas caracter&iacute;sticas y finalidad est&aacute;n detalladas en la Pol&iacute;tica</p>
<p>de Cookies.</p>
<p>Informaci&oacute;n recopilada</p>
<p>Contacto: Consultas, comentarios, sugerencias, ideas, etc. Informaci&oacute;n personal recabada a trav&eacute;s del</p>
<p>formulario que incluye, a t&iacute;tulo enunciativo, pero no limitativo, su nombre, apellidos, e-mail(s), fecha de</p>
<p>nacimiento, domicilio, poblaci&oacute;n, provincia, tel&eacute;fonos.</p>
<p>Newsletter/informaci&oacute;n comercial: al suscribirse a la newsletter y al env&iacute;o de informaci&oacute;n comercial, los</p>
<p>usuarios proporcionan sus emails y aceptan recibir por correo electr&oacute;nico la newsletter y/o</p>
<p>informaciones comerciales de la Federaci&oacute;n.</p>
<p>&Aacute;rea privada: informaci&oacute;n del Asociado</p>
<p>Legitimaci&oacute;n para el tratamiento de los datos:</p>
<p>La base legal para el tratamiento de los datos personales de los usuarios se desglosa a continuaci&oacute;n, y</p>
<p>reside en cualquier caso en el propio consentimiento del interesado:</p>
<p>Legitimaci&oacute;n por consentimiento: cuando el Usuario realiza cualquier consulta o comentario</p>
<p>utilizando el formulario de contacto</p>
<p>Legitimaci&oacute;n por consentimiento: cuando el Usuario proporciona sus datos voluntariamente para</p>
<p>unirse a la Federaci&oacute;n.</p>
<p><br></p>
<p>Federaci&oacute;n de Hosteler&iacute;a de Burgos</p>
<p>Plaza Castilla n. 1 2a, - 09003 Burgos</p>
<p>Legitimaci&oacute;n por consentimiento: cuando el Usuario proporciona sus datos voluntariamente para</p>
<p>recibir la newsletter y/o informaci&oacute;n comercial de la Federaci&oacute;n.</p>
<p>Legitimaci&oacute;n por consentimiento: cuando el Usuario realiza gestiones desde su &aacute;rea Privada.</p>
<p>Legitimaci&oacute;n por consentimiento: cuando el Usuario acepta el tratamiento de cookies de terceros</p>
<p>Conservaci&oacute;n de la informaci&oacute;n</p>
<p>Los datos relativos a las consultas se conservar&aacute;n durante no m&aacute;s tiempo del necesario para mantener</p>
<p>el fin del tratamiento o existan prescripciones legales que dictaminen su custodia y cuando ya no sea</p>
<p>necesario para ello, se suprimir&aacute;n con medidas de seguridad adecuadas para garantizar la</p>
<p>anonimizaci&oacute;n de los datos o la destrucci&oacute;n total de los mismos.</p>
<p>Los datos de car&aacute;cter personal que el Usuario proporciona al solicitar la newsletter y/o el env&iacute;o de</p>
<p>informaci&oacute;n comercial de la Federaci&oacute;n se conservar&aacute;n mientras no se solicite su supresi&oacute;n por el</p>
<p>Usuario. Dicha solicitud conllevar&aacute; su baja como Usuario de este servicio.</p>
<p>Los datos de car&aacute;cter personal del Asociado en su &aacute;rea privada se conservar&aacute;n mientras el usuario no</p>
<p>solicite la cancelaci&oacute;n de sus datos de registro y como consecuencia de ello la baja de la Federaci&oacute;n.</p>
<p>Destinatario de los datos</p>
<p>Sus datos personales no ser&aacute;n cedidos a terceros, salvo que sean requeridos por ley o por las</p>
<p>autoridades competentes. Asimismo se informa al Usuario que la Federaci&oacute;n tiene contratada servicios</p>
<p>de mantenimiento y hosting, a trav&eacute;s de contratos de encargo del tratamiento para dar soporte a los</p>
<p>fines de tratamiento indicados.</p>
<p>Derechos</p>
<p>El interesado tiene derecho a:</p>
<p>Solicitar el acceso a sus datos personales.</p>
<p>Solicitar su rectificaci&oacute;n o supresi&oacute;n.</p>
<p>Solicitar la limitaci&oacute;n de su tratamiento.</p>
<p>Oponerse al tratamiento.</p>
<p>Solicitar la portabilidad de los datos.</p>
<p>Los interesados podr&aacute;n acceder a sus datos personales, as&iacute; como a solicitar la rectificaci&oacute;n de los</p>
<p>datos inexactos o, en su caso, solicitar su supresi&oacute;n cuando, entre otros motivos, los datos ya no sean</p>
<p>necesarios para los fines que fueron recogidos. En determinadas circunstancias, los interesados</p>
<p>podr&aacute;n solicitar la limitaci&oacute;n del tratamiento de sus datos, en cuyo caso &uacute;nicamente los conservar&eacute;</p>
<p>para el ejercicio o la defensa de reclamaciones.</p>
<p>En determinadas circunstancias y por motivos relacionados con su situaci&oacute;n particular, los interesados</p>
<p>podr&aacute;n oponerse al tratamiento de sus datos. La Federaci&oacute;n dejar&aacute; de tratar los datos, salvo por</p>
<p>motivos leg&iacute;timos imperiosos, o el ejercicio o la defensa de posibles reclamaciones. C&oacute;mo interesado,</p>
<p>tiene derecho a recibir los datos personales que le incumban en un formato estructurado, de uso</p>
<p>com&uacute;n y lectura mec&aacute;nica, y a transmitirlos a otro responsable del tratamiento cuando:</p>
<p>El tratamiento est&eacute; basado en el consentimiento.</p>
<p><br></p>
<p>Federaci&oacute;n de Hosteler&iacute;a de Burgos</p>
<p>Plaza Castilla n. 1 2a, - 09003 Burgos</p>
<p><br></p>
<p>Los datos hayan sido facilitados por la persona interesada.</p>
<p>El tratamiento se efect&uacute;e por medios automatizados.</p>
<p>Al ejercer tu derecho a la portabilidad de los datos, tendr&aacute; derecho a que los datos personales se</p>
<p>transmitan directamente de responsable a responsable cuando sea t&eacute;cnicamente posible.</p>
<p>Los interesados tambi&eacute;n tendr&aacute;n derecho a la tutela judicial efectiva y a presentar una reclamaci&oacute;n</p>
<p>ante la autoridad de control, en este caso, la Agencia Espa&ntilde;ola de Protecci&oacute;n de Datos, si consideran</p>
<p>que el tratamiento de datos personales que le conciernen infringe el Reglamento.</p>
<p>Datos de contacto para ejercer sus derechos:</p>
<p>Federaci&oacute;n de Hosteler&iacute;a de Burgos. Plaza Castilla n. 1 2a, - 09003 Burgos. E-mail:</p>
<p><a data-fr-linked="true" href="mailto:federacionhosteleriaburgos@gmail.es">federacionhosteleriaburgos@gmail.es</a></p>
<p>Secreto y Seguridad de los datos</p>
<p>La Federaci&oacute;n se compromete en el uso y tratamiento de los datos incluidos personales de los</p>
<p>usuarios, respetando su confidencialidad y a utilizarlos de acuerdo con la finalidad del mismo, as&iacute; como</p>
<p>a dar cumplimiento a su obligaci&oacute;n de guardarlos y adaptar todas las medidas para evitar la alteraci&oacute;n,</p>
<p>p&eacute;rdida, tratamiento o acceso no autorizado, de conformidad con lo establecido en la normativa vigente</p>
<p>de protecci&oacute;n de datos.</p>
<p>Esta web incluye un certificado SSL. Se trata de un protocolo de seguridad que hace que tus datos</p>
<p>viajen de manera &iacute;ntegra y segura, es decir, la transmisi&oacute;n de los datos entre un servidor y usuario</p>
<p>web, y en retroalimentaci&oacute;n, es totalmente cifrada o encriptada.</p>
<p>La Federaci&oacute;n no puede garantizar la absoluta inexpugnabilidad de la red Internet y por tanto la</p>
<p>violaci&oacute;n de los datos mediante accesos fraudulentos a ellos por parte de terceros.</p>` }} />
        </div>
      </div>
      <Footer />
    </div>
  );
}
