import { Collapse } from "antd";
import styles from "../index.module.css";
import Footer from "../../Footer";
import NavBar from "../../NavBar";

const { Panel } = Collapse;

const Preguntas = () => {
  return (
    <div style={{ backgroundColor: "#f4f5f5" }}>
      <NavBar home={false} />
      <div className={styles.containers_hepls}>
        <div className={styles.cookie}>
          <Collapse accordion>
            <Panel header="¿Cómo me uno a la Federación?" key="1">
              <p>
                En nuestra misma web tiene un boton Unirme llena el formularío y nos pondremos en contacto contigo.
              </p>
            </Panel>
            <Panel
              header="¿Que se hace en la Federación?"
              key="2"
            >
              <p>
                Asesoramiento y elaboración de contratos de arrendamiento, actualización de rentas, traspasos, desahucios.... Cursos y reuniones profesionales Cursos de reciclaje y formación continua de empresas Seguros de accidentes para los trabajadores en condiciones ventajosas Compra agrupada de la energía. Circulares informativas sobre novedades de interés. Servicios de documentación, información, difusión, propaganda, etc. Gestión y planificación de actividades.
              </p>
            </Panel>
            <Panel header="Que beneficios obtengo?" key="3">
              <p>
                Tramitación de recursos, alegaciones, pliegos de descargo en todo tipo de expediente sancionadores, motivados por: Problemas de ruidos, horarios de cierre y acceso a menores. Inspecciones de Turismo, Sanidad, etc. Reclamaciones de clientes. Denuncias y seguimiento del intrusismo y competencia desleal. Reclamaciones y gestiones de cobro de clientes morosos.
              </p>
            </Panel>
            <Panel header="¿Qué es la Federación?" key="4">
              <p>
                La Federación Provincial de Hostelería es una organización de ámbito provincial que agrupa y representa a una buena parte del sector hostelero de Burgos y su provincia, siendo su deseo, contar con la participación de todos los empresarios de la provincia.
              </p>
            </Panel>
            <Panel header="¿Dónde esta ubicada?" key="5">
              <p>Burgos, España Plaza de Castilla 1, 2o. 09003.</p>
            </Panel>
            <Panel header="¿Cuales son los beneficios?" key="6">
              <p>
                Altas, Bajas y modificaciones del Impuesto de Actividades económicas y Censo de Actividades. Constitución de Sociedades Civiles y Comunidades de Bienes. Liquidación de las retenciones por alquiler de bienes inmuebles. Declaración del Impuesto de la Renta de las Personas Físicas y del Patrimonio. Solicitud y transmisión del Permiso Municipal de Apertura y Licencia de Actividad. Asesoramiento y recursos ante la Administración Tributaria.
              </p>
            </Panel>
          </Collapse>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Preguntas;
