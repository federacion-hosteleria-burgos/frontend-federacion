import styles from "../index.module.css";
import Footer from "../../Footer";
import NavBar from "../../NavBar";

export default function TermAndCondition() {
  return (
    <div>
      <NavBar home={false} />
      <div className={styles.containers_hepls}>
        <div className={styles.cookies}>
        <div dangerouslySetInnerHTML={{ __html: `<p><strong>Aviso legal</strong></p>
<p><br></p>
<p>Federaci&oacute;n de Hosteler&iacute;a de Burgos, responsable del sitio web, &quot;en adelante Federaci&oacute;n&quot;, pone a</p>
<p>disposici&oacute;n de los usuarios el presente documento, con el que pretende dar cumplimiento a las</p>
<p>obligaciones dispuestas en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la</p>
<p>Informaci&oacute;n y de Comercio Electr&oacute;nico (LSSICE), BOE N.o 166, as&iacute; como informar a todos los usuarios</p>
<p>del sitio web respecto a cu&aacute;les son las condiciones de uso.</p>
<p>Toda persona que acceda a este sitio web asume el papel de Usuario, comprometi&eacute;ndose a la</p>
<p>observancia y cumplimiento riguroso de las disposiciones aqu&iacute; dispuestas, as&iacute; como a cualquier otra</p>
<p>disposici&oacute;n legal que fuera de aplicaci&oacute;n.</p>
<p>A trav&eacute;s del presente sitio web, la Federaci&oacute;n da a conocer informaci&oacute;n sobre sus valores, servicios a</p>
<p>los asociados, informaci&oacute;n tur&iacute;stica, acceso a revistas y cualquier otro tipo de noticias del sector que</p>
<p>pueden ser de inter&eacute;s.</p>
<p>La Federaci&oacute;n se reserva el derecho de modificar cualquier tipo de informaci&oacute;n que pudiera aparecer</p>
<p>en el sitio web, sin que exista obligaci&oacute;n de preavisar o poner en conocimiento de los usuarios dichas</p>
<p>obligaciones, entendi&eacute;ndose como suficiente la publicaci&oacute;n en el presente sitio web</p>
<p>1. DATOS IDENTIFICATIVOS</p>
<p>Nombre de dominio: federacionhosteleriaburgos.es</p>
<p>Nombre comercial: Federaci&oacute;n de Hosteler&iacute;a de Burgos</p>
<p>Denominaci&oacute;n social: Federaci&oacute;n de Hosteler&iacute;a de Burgos</p>
<p>CIF: G0918136</p>
<p>Domicilio social: Plaza Castilla n. 1 2a, 09003 Burgos.</p>
<p>Tel&eacute;fono: 947 209 610</p>
<p>E-mail: <a data-fr-linked="true" href="mailto:federacionhosteleriaburgos@gmail.es">federacionhosteleriaburgos@gmail.es</a></p>
<p>2. DERECHOS DE PROPIEDAD INTELECTUAL E INDUSTRIAL</p>
<p>El sitio web, incluyendo a t&iacute;tulo enunciativo pero no limitativo su programaci&oacute;n, edici&oacute;n, compilaci&oacute;n y</p>
<p>dem&aacute;s elementos necesarios para su funcionamiento, los dise&ntilde;os, logotipos, texto y/o gr&aacute;ficos, son</p>
<p>propiedad de la Federaci&oacute;n o, si es el caso, dispone de licencia o autorizaci&oacute;n expresa por parte de los</p>
<p>autores. Todos los contenidos del sitio web se encuentran debidamente protegidos por la normativa de</p>
<p>propiedad intelectual e industrial, as&iacute; como inscritos en los registros p&uacute;blicos correspondientes.</p>
<p>Independientemente de la finalidad para la que fueran destinados, la reproducci&oacute;n total o parcial, uso,</p>
<p>explotaci&oacute;n, distribuci&oacute;n y comercializaci&oacute;n, requiere en todo caso la autorizaci&oacute;n escrita previa por</p>
<p>parte de la Federaci&oacute;n. Cualquier uso no autorizado previamente se considera un incumplimiento</p>
<p>grave de los derechos de propiedad intelectual o industrial del autor.</p>
<p>Los dise&ntilde;os, logotipos, texto y/o gr&aacute;ficos ajenos a la Federaci&oacute;n y que pudieran aparecer en el sitio</p>
<p>web, pertenecen a sus respectivos propietarios, siendo ellos mismos responsables de cualquier posible</p>
<p><br></p>
<p>controversia que pudiera suscitarse respecto a los mismos. La Federaci&oacute;n autoriza expresamente a</p>
<p>que terceros puedan redirigir directamente a los contenidos concretos del sitio web, y en todo caso</p>
<p>redirigir al sitio web principal de <a data-fr-linked="true" href="//www.federacionhosteleriaburgos.es">www.federacionhosteleriaburgos.es</a>.</p>
<p>La Federaci&oacute;n reconoce a favor de sus titulares los correspondientes derechos de propiedad</p>
<p>intelectual e industrial, no implicando su sola menci&oacute;n o aparici&oacute;n en el sitio web la existencia de</p>
<p>derechos o responsabilidad alguna sobre los mismos, como tampoco respaldo, patrocinio o</p>
<p>recomendaci&oacute;n por parte del mismo.</p>
<p>Para realizar cualquier tipo de observaci&oacute;n respecto a posibles incumplimientos de los derechos de</p>
<p>propiedad intelectual o industrial, as&iacute; como sobre cualquiera de los contenidos del sitio web, puede</p>
<p>hacerlo a trav&eacute;s del correo electr&oacute;nico: <a data-fr-linked="true" href="mailto:federacionhosteleriaburgos@gmail.es">federacionhosteleriaburgos@gmail.es</a>.</p>
<p>3. OBLIGACIONES Y RESPONSABILIDADES</p>
<p>El Usuario se compromete a:</p>
<p>a) Hacer un uso adecuado y l&iacute;cito del sitio web as&iacute; como de los contenidos y servicios, de conformidad</p>
<p>con: (i) la legislaci&oacute;n aplicable en cada momento; (ii) las Condiciones Generales de Uso del sitio web;</p>
<p>(iii) la moral y buenas costumbres generalmente aceptadas y (iv) el orden p&uacute;blico.</p>
<p>b) Proveerse de todos los medios y requerimientos t&eacute;cnicos que se precisen para acceder al sitio web.</p>
<p>c) Facilitar informaci&oacute;n veraz al cumplimentar con sus datos de car&aacute;cter personal los formularios</p>
<p>contenidos en el sitio web y a mantenerlos actualizados en todo momento de forma que responda, en</p>
<p>cada momento, a la situaci&oacute;n real del usuario. El Usuario ser&aacute; el &uacute;nico responsable de las</p>
<p>manifestaciones falsas o inexactas que realice y de los perjuicios que cause a la Federaci&oacute;n o a</p>
<p>terceros por la informaci&oacute;n que facilite.</p>
<p>Igualmente, el Usuario deber&aacute; abstenerse de</p>
<p>1) Acceder o intentar acceder a recursos o &aacute;reas restringidas del sitio web, sin cumplir las condiciones</p>
<p>exigidas para dicho acceso.</p>
<p>2) Provocar da&ntilde;os en los sistemas f&iacute;sicos o l&oacute;gicos del sitio web, de sus proveedores o de terceros.</p>
<p>3) Introducir o difundir en la red virus inform&aacute;ticos o cualesquiera otros sistemas f&iacute;sicos o l&oacute;gicos que</p>
<p>sean susceptibles de provocar da&ntilde;os en los sistemas f&iacute;sicos</p>
<p>4) Intentar acceder, utilizar y/o manipular los datos de la Federaci&oacute;n, terceros proveedores y otros</p>
<p>usuarios.</p>
<p>5) Obtener e intentar obtener los contenidos empleando para ello medios o procedimientos distintos de</p>
<p>los que, seg&uacute;n los casos, se hayan puesto a su disposici&oacute;n a este efecto o se hayan indicado</p>
<p>expresamente en las p&aacute;ginas web donde se encuentren los contenidos o, en general, de los que se</p>
<p>empleen habitualmente en Internet por no entra&ntilde;ar un riesgo de da&ntilde;o o inutilizaci&oacute;n del sitio web y/o</p>
<p>de los contenidos.</p>
<p>6) En particular, y a t&iacute;tulo meramente indicativo y no exhaustivo, el Usuario se compromete a no</p>
<p>transmitir, difundir o poner a disposici&oacute;n de terceros informaciones, cualquier clase de material que:</p>
<p><br></p>
<p>-De cualquier forma sea contrario, menosprecie o atente contra los derechos fundamentales y las</p>
<p>libertades p&uacute;blicas reconocidas constitucionalmente, en los Tratados Internacionales y en el resto de la</p>
<p>legislaci&oacute;n vigente.</p>
<p>-Induzca, incite o promueva actuaciones delictivas, denigratorias, difamatorias, violentas o, en general,</p>
<p>contrarias a la ley, a la moral, a las buenas costumbres generalmente aceptadas o al orden p&uacute;blico.</p>
<p>-Incorpore, ponga a disposici&oacute;n o permita acceder a productos, elementos, mensajes y/o servicios</p>
<p>delictivos, violentos, ofensivos, nocivos, degradantes o, en general, contrarios a la ley, a la moral y a</p>
<p>las buenas costumbres generalmente aceptadas o al orden p&uacute;blico.</p>
<p>-Induzca o pueda inducir a un estado inaceptable de ansiedad o temor.</p>
<p>-Sea contrario al honor, a la intimidad personal y familiar o a la propia imagen de las personas</p>
<p>-Constituya cualquier tipo de publicidad.</p>
<p>Si de manera negligente o dolosa incumpliera cualquiera de las obligaciones establecidas en las</p>
<p>presentes Condiciones Generales de Uso, responder&aacute; por todos los da&ntilde;os y perjuicios que de dicho</p>
<p>incumplimiento pudieran derivarse para la Federaci&oacute;n. Asimismo la Federaci&oacute;n no garantiza el acceso</p>
<p>continuado, ni la correcta visualizaci&oacute;n, descarga o utilidad de los elementos e informaciones</p>
<p>contenidas en su web, que pueden verse impedidos, dificultados o interrumpidos por factores o</p>
<p>circunstancias que est&aacute;n fuera de su control</p>
<p>4. USO DE COOKIES</p>
<p>Este sitio web puede utilizar cookies t&eacute;cnicas (peque&ntilde;os archivos de informaci&oacute;n que el servidor env&iacute;a</p>
<p>al ordenador de quien accede a la p&aacute;gina) para llevar a cabo determinadas funciones que son</p>
<p>consideradas imprescindibles para el correcto funcionamiento y visualizaci&oacute;n del sitio. Las cookies</p>
<p>utilizadas tienen, en todo caso, car&aacute;cter temporal, con la &uacute;nica finalidad de hacer m&aacute;s eficaz la</p>
<p>navegaci&oacute;n, y desaparecen al terminar la sesi&oacute;n del Usuario. En ning&uacute;n caso, estas cookies</p>
<p>proporcionan por s&iacute; mismas datos de car&aacute;cter personal y no se utilizar&aacute;n para la recogida de los</p>
<p>mismos.</p>
<p>Mediante el uso de cookies tambi&eacute;n es posible que el servidor donde se encuentra la web reconozca el</p>
<p>navegador utilizado por el usuario con la finalidad de que la navegaci&oacute;n sea m&aacute;s sencilla, permitiendo,</p>
<p>por ejemplo, el acceso de los usuarios que se hayan registrado previamente a las &aacute;reas, servicios,</p>
<p>promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita.</p>
<p>Tambi&eacute;n se pueden utilizar para medir la audiencia, par&aacute;metros de tr&aacute;fico, controlar el progreso y</p>
<p>n&uacute;mero de entradas, etc., siendo en estos casos cookies prescindibles t&eacute;cnicamente, pero</p>
<p>beneficiosas para el Usuario. Este sitio web no instalar&aacute; cookies prescindibles sin el consentimiento</p>
<p>previo del Usuario.</p>
<p>Este sitio web utiliza cookies propias y de terceros para fines anal&iacute;ticos y para mostrarle publicidad</p>
<p>personalizada en base a un perfil elaborado a partir de sus h&aacute;bitos de navegaci&oacute;n (por ejemplo,</p>
<p>p&aacute;ginas visitadas). A todo Usuario que visita la web se le informa del uso de estas cookies mediante un</p>
<p>banner flotante. En el caso de aceptar su uso, el banner desaparecer&aacute;, aunque en todo momento</p>
<p>podr&aacute; revocar el consentimiento y obtener m&aacute;s informaci&oacute;n consultando nuestra Pol&iacute;tica de cookies.</p>
<p><br></p>
<p>El Usuario tiene la posibilidad de configurar su navegador para ser alertado de la recepci&oacute;n de cookies</p>
<p>y para impedir su instalaci&oacute;n en su equipo. Por favor, consulte las instrucciones de su navegador para</p>
<p>ampliar esta informaci&oacute;n.</p>
<p>5. POL&Iacute;TICA DE ENLACES</p>
<p>Desde el sitio web, es posible que se redirija a contenidos de terceros sitios web. Dado que la</p>
<p>Federaci&oacute;n no puede controlar siempre los contenidos introducidos por terceros en sus respectivos</p>
<p>sitios web, no asume ning&uacute;n tipo de responsabilidad respecto a dichos contenidos. En todo caso,</p>
<p>proceder&aacute; a la retirada inmediata de cualquier contenido que pudiera contravenir la legislaci&oacute;n nacional</p>
<p>o internacional, la moral o el orden p&uacute;blico, procediendo a la retirada inmediata de la redirecci&oacute;n a</p>
<p>dicho sitio web, poniendo en conocimiento de las autoridades competentes el contenido en cuesti&oacute;n.</p>
<p>La Federaci&oacute;n no se hace responsable de la informaci&oacute;n y contenidos almacenados, a t&iacute;tulo</p>
<p>enunciativo pero no limitativo, en foros, chats, generadores de blogs, comentarios, redes sociales o</p>
<p>cualquier otro medio que permita a terceros publicar contenidos de forma independiente en del sitio</p>
<p>web de la Federaci&oacute;n. Sin embargo, y en cumplimiento de lo dispuesto en los art&iacute;culos 11 y 16 de la</p>
<p>LSSICE, se pone a disposici&oacute;n de todos los usuarios, autoridades y fuerzas de seguridad, colaborando</p>
<p>de forma activa en la retirada o, en su caso, bloqueo de todos aquellos contenidos que puedan afectar</p>
<p>o contravenir la legislaci&oacute;n nacional o internacional, los derechos de terceros o la moral y el orden</p>
<p>p&uacute;blico. En caso de que el Usuario considere que existe en el sitio web alg&uacute;n contenido que pudiera</p>
<p>ser susceptible de esta clasificaci&oacute;n, se ruega lo notifique de forma inmediata al administrador del sitio</p>
<p>web.</p>
<p>Este sitio web se ha revisado y probado para que funcione correctamente. En principio, puede</p>
<p>garantizarse el correcto funcionamiento los 365 d&iacute;as del a&ntilde;o, 24 horas al d&iacute;a. Sin embargo, la</p>
<p>Federaci&oacute;n no descarta la posibilidad de que existan ciertos errores de programaci&oacute;n, o que</p>
<p>acontezcan causas de fuerza mayor, cat&aacute;strofes naturales, huelgas o circunstancias semejantes que</p>
<p>hagan imposible el acceso al sitio web.</p>
<p>6. PROTECCI&Oacute;N DE DATOS</p>
<p>Sus datos ser&aacute;n tratados con la finalidad de atender los distintos tipos de relaciones que puedan surgir</p>
<p>entre los usuarios del sitio web y la Federaci&oacute;n, en virtud de los distintos tipos de servicios que se</p>
<p>pueden ofrecer desde el presente sitio web.</p>
<p>Para utilizar los formularios del sitio web, se les puede solicitar a los usuarios proporcionar previamente</p>
<p>ciertos datos de car&aacute;cter personal. La Federaci&oacute;n tratar&aacute; los Datos Personales en cumplimiento con el</p>
<p>Reglamento General de Protecci&oacute;n de Datos de Europa UE 679/2016 y con la Ley Org&aacute;nica 3/2018 de</p>
<p>5 diciembre, de Protecci&oacute;n de Datos Personales y Garant&iacute;a de los Derechos Digitales, as&iacute; como las</p>
<p>normativas que le sean de aplicaci&oacute;n, en su caso. Para conocer m&aacute;s detalles sobre el tratamiento de</p>
<p>los datos, el Usuario puede acceder a la Pol&iacute;tica de Privacidad o Protecci&oacute;n de Datos que presenta</p>
<p>este sitio web.</p>
<p>Direcciones IP</p>
<p>Los servidores del sitio web podr&aacute;n detectar de manera autom&aacute;tica la direcci&oacute;n IP y el nombre de</p>
<p>dominio utilizados por el usuario. Una direcci&oacute;n IP es un n&uacute;mero asignado autom&aacute;ticamente a un</p>
<p><br></p>
<p>ordenador cuando este se conecta a Internet. Toda esta informaci&oacute;n se registra en un fichero de</p>
<p>actividad del servidor debidamente inscrito que permite el posterior procesamiento de los datos con el</p>
<p>fin de obtener mediciones &uacute;nicamente estad&iacute;sticas que permitan conocer el n&uacute;mero de impresiones de</p>
<p>p&aacute;ginas, el n&uacute;mero de visitas realizadas a los servidores web, el orden de visitas, el punto de acceso,</p>
<p>etc.</p>
<p>7. LEY APLICABLE Y JURISDICCI&Oacute;N</p>
<p>Para la resoluci&oacute;n de todas las controversias o cuestiones relacionadas con el presente sitio web o de</p>
<p>las actividades en &eacute;l desarrolladas, ser&aacute; de aplicaci&oacute;n la legislaci&oacute;n espa&ntilde;ola, a la que se someten</p>
<p>expresamente las partes as&iacute; como los Juzgados y Tribunales que en casa caso sean competentes</p>` }} />
        </div>
      </div>
      <Footer />
    </div>
  );
}
