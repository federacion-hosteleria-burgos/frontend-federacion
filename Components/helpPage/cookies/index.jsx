import styles from "../index.module.css";
import Footer from "../../Footer";
import NavBar from "../../NavBar";

export default function Cookies() {
  return (
    <div>
      <NavBar home={false} />
      <div className={styles.containers_hepls}>
        <div className={styles.entry_content}>
          <blockquote className={styles.cookies}>
          <div dangerouslySetInnerHTML={{ __html: `<p>INFORMACI&Oacute;N SOBRE COOKIES</p>
<p><br></p>
<p>Conforme con la Ley 34/2002, de 11 de julio, de servicios de la sociedad de la informaci&oacute;n y de comercio electr&oacute;nico</p>
<p>(LSSI), en relaci&oacute;n con el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016,</p>
<p>General de Protecci&oacute;n de Datos (GDPR) y la Ley Org&aacute;nica 3/2018, de 5 de diciembre, de Protecci&oacute;n de Datos y Garant&iacute;a</p>
<p>de los Derechos Digitales (LOPDGDD), es obligado obtener el consentimiento expreso del usuario de todas las p&aacute;ginas</p>
<p>web que usan cookies prescindibles, antes de que este navegue por ellas.</p>
<p><br></p>
<p>&iquest;QU&Eacute; SON LAS COOKIES?</p>
<p><br></p>
<p>Las cookies y otras tecnolog&iacute;as similares tales como local shared objects, flash cookies o p&iacute;xeles, son herramientas</p>
<p>empleadas por los servidores Web para almacenar y recuperar informaci&oacute;n acerca de sus visitantes, as&iacute; como para ofrecer</p>
<p>un correcto funcionamiento del sitio.</p>
<p>Mediante el uso de estos dispositivos se permite al servidor Web recordar algunos datos concernientes al usuario, como</p>
<p>sus preferencias para la visualizaci&oacute;n de las p&aacute;ginas de ese servidor, nombre y contrase&ntilde;a, productos que m&aacute;s le</p>
<p>interesan, etc.</p>
<p><br></p>
<p>COOKIES AFECTADAS POR LA NORMATIVA Y COOKIES EXCEPTUADAS</p>
<p><br></p>
<p>Seg&uacute;n la directiva de la UE, las cookies que requieren el consentimiento informado por parte del usuario son las cookies de</p>
<p>anal&iacute;tica y las de publicidad y afiliaci&oacute;n, quedando exceptuadas las de car&aacute;cter t&eacute;cnico y las necesarias para el</p>
<p>funcionamiento del sitio web o la prestaci&oacute;n de servicios expresamente solicitados por el usuario.</p>
<p><br></p>
<p>TIPOS DE COOKIES</p>
<p><br></p>
<p>SEG&Uacute;N LA FINALIDAD</p>
<p>Cookies t&eacute;cnicas y funcionales: son aquellas que permiten al usuario la navegaci&oacute;n a trav&eacute;s de una p&aacute;gina web,</p>
<p>plataforma o aplicaci&oacute;n y la utilizaci&oacute;n de las diferentes opciones o servicios que en ella existan.</p>
<p>Cookies anal&iacute;ticas: son aquellas que permiten al responsable de las mismas el seguimiento y an&aacute;lisis del</p>
<p>comportamiento de los usuarios de los sitios web a los que est&aacute;n vinculadas. La informaci&oacute;n recogida mediante</p>
<p>este tipo de cookies se utiliza en la medici&oacute;n de la actividad de los sitios web, aplicaci&oacute;n o plataforma y para la</p>
<p>elaboraci&oacute;n de perfiles de navegaci&oacute;n de los usuarios de dichos sitios, aplicaciones y plataformas, con el fin de</p>
<p>introducir mejoras en funci&oacute;n del an&aacute;lisis de los datos de uso que hacen los usuarios del servicio.</p>
<p>Cookies publicitarias: son aquellas que permiten la gesti&oacute;n, de la forma m&aacute;s eficaz posible, de los espacios</p>
<p>publicitarios que, en su caso, el editor haya incluido en una p&aacute;gina web, aplicaci&oacute;n o plataforma desde la que</p>
<p>presta el servicio solicitado en base a criterios como el contenido editado o la frecuencia en la que se muestran los</p>
<p>anuncios.</p>
<p>Cookies de publicidad comportamental: recogen informaci&oacute;n sobre las preferencias y elecciones personales del</p>
<p>usuario (retargeting) para permitir la gesti&oacute;n, de la forma m&aacute;s eficaz posible, de los espacios publicitarios que, en</p>
<p>su caso, el editor haya incluido en una p&aacute;gina web, aplicaci&oacute;n o plataforma desde la que presta el servicio</p>
<p>solicitado.</p>
<p>Cookies sociales: son establecidas por las plataformas de redes sociales en los servicios para permitirle compartir</p>
<p>contenido con sus amigos y redes. Las plataformas de medios sociales tienen la capacidad de rastrear su actividad&nbsp; en l&iacute;nea fuera de los Servicios. Esto puede afectar al contenido y los mensajes que ve en otros servicios que visita.</p>
<p>Cookies de afiliados: permiten hacer un seguimiento de las visitas procedentes de otras webs, con las que el sitio</p>
<p>web establece un contrato de afiliaci&oacute;n (empresas de afiliaci&oacute;n).</p>
<p>Cookies de seguridad: almacenan informaci&oacute;n cifrada para evitar que los datos guardados en ellas sean</p>
<p>vulnerables a ataques maliciosos de terceros.</p>
<p>SEG&Uacute;N LA PROPIEDAD</p>
<p>Cookies propias: son aquellas que se env&iacute;an al equipo terminal del usuario desde un equipo o dominio gestionado</p>
<p>por el propio editor y desde el que se presta el servicio solicitado por el usuario.</p>
<p>Cookies de terceros: son aquellas que se env&iacute;an al equipo terminal del usuario desde un equipo o dominio que no</p>
<p>es gestionado por el editor, sino por otra entidad que trata los datos obtenidos trav&eacute;s de las cookies.</p>
<p>SEG&Uacute;N EL PLAZO DE CONSERVACI&Oacute;N</p>
<p>Cookies de sesi&oacute;n: son un tipo de cookies dise&ntilde;adas para recabar y almacenar datos mientras el usuario accede</p>
<p>a una p&aacute;gina web.</p>
<p>Cookies persistentes: son un tipo de cookies en el que los datos siguen almacenados en el terminal y pueden ser</p>
<p>accedidos y tratados durante un per&iacute;odo definido por el responsable de la cookie, y que puede ir de unos minutos a</p>
<p>varios a&ntilde;os.</p>
<p><br></p>
<p>TRATAMIENTO DE DATOS PERSONALES</p>
<p><br></p>
<p>Federaci&oacute;n de Hosteler&iacute;a de Burgos es el Responsable del tratamiento de los datos personales del Interesado y le</p>
<p>informa de que estos datos ser&aacute;n tratados de conformidad con lo dispuesto en el Reglamento (UE) 2016/679, de 27 de</p>
<p>abril de 2016 (GDPR), por lo que se le facilita la siguiente informaci&oacute;n del tratamiento:</p>
<p>Fines del tratamiento: seg&uacute;n se especifica en el apartado de cookies que se utilizan en este sitio web.</p>
<p>Legitimaci&oacute;n del tratamiento: salvo en los casos en los que resulte necesario para la navegaci&oacute;n por la web, por</p>
<p>consentimiento del interesado (art. 6.1 GDPR).</p>
<p>Criterios de conservaci&oacute;n de los datos: seg&uacute;n se especifica en el apartado de cookies utilizadas en la web.</p>
<p>Comunicaci&oacute;n de los datos: no se comunicar&aacute;n los datos a terceros, excepto en cookies propiedad de terceros o por</p>
<p>obligaci&oacute;n legal.</p>
<p>Derechos que asisten al Interesado:</p>
<p>- Derecho a retirar el consentimiento en cualquier momento.</p>
<p>- Derecho de acceso, rectificaci&oacute;n, portabilidad y supresi&oacute;n de sus datos, y de limitaci&oacute;n u oposici&oacute;n a su tratamiento.</p>
<p>- Derecho a presentar una reclamaci&oacute;n ante la Autoridad de control (<a data-fr-linked="true" href="//www.aepd.es">www.aepd.es</a>) si considera que el tratamiento no se</p>
<p>ajusta a la normativa vigente.</p>
<p>Datos de contacto para ejercer sus derechos:</p>
<p>Federaci&oacute;n de Hosteler&iacute;a de Burgos. Plaza Castilla n. 1 2a, - 09003 Burgos . E-mail: <a data-fr-linked="true" href="mailto:federacionhosteleriaburgos@gmail.es">federacionhosteleriaburgos@gmail.es</a></p>
<p><br></p>
<p>COOKIES UTILIZADAS EN ESTE SITIO WEB</p>
<p><br></p>
<p>COOKIES CONTROLADAS POR EL EDITOR</p>
<p><br></p>
<p>T&eacute;cnicas y funcionales</p>
<p><br></p>
<p>Propiedad Cookie Finalidad Plazo</p>
<p>autonomosyemprende</p>
<p>dor.es _ga_B35FCHPXHZ Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>autonomosyemprende</p>
<p>dor.es addtl_consent Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>autonomosyemprende</p>
<p>dor.es euconsent-v2 Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>autonomosyemprende</p>
<p>dor.es ivbsdid Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en 5 meses</p>
<p><br></p>
<p>autonomosyemprende</p>
<p>dor.es qcSxc Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>diariodeburgos.es _ga_KV8L03B82R Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>diariodeburgos.es _ga_V2TWFYNGR</p>
<p><br></p>
<p>9</p>
<p><br></p>
<p>Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>diariodeburgos.es addtl_consent Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>diariodeburgos.es euconsent-v2 Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>diariodeburgos.es qcSxc Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>foodretail.es _ga_NRC0WJCCF</p>
<p><br></p>
<p>W</p>
<p><br></p>
<p>Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p><br></p>
<p>foodretail.es NewsletterFirstEnt</p>
<p><br></p>
<p>ry</p>
<p><br></p>
<p>Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p>servicios del sitio web en 26 minutos</p>
<p><br></p>
<p>foodretail.es qcSxc Cookie necesaria para la utilizaci&oacute;n de las opciones y</p>
<p><br></p>
<p>servicios del sitio web en un a&ntilde;o</p>
<p>Anal&iacute;ticas</p>
<p><br></p>
<p>Propiedad Cookie Finalidad Plazo</p>
<p>autonomosyemprende</p>
<p>dor.es _ga ID utiliza para identificar a los usuarios en un a&ntilde;o</p>
<p>diariodeburgos.es _ga ID utiliza para identificar a los usuarios en un a&ntilde;o</p>
<p>foodretail.es _ga ID utiliza para identificar a los usuarios en un a&ntilde;o&nbsp;</p>
<p>foodretail.es _gid ID utiliza para identificar a los usuarios durante 24 horas</p>
<p><br></p>
<p>despu&eacute;s de la &uacute;ltima actividad en 22 horas</p>
<p>pollogomez.com _ga ID utiliza para identificar a los usuarios en un a&ntilde;o</p>
<p>pollogomez.com _gid ID utiliza para identificar a los usuarios durante 24 horas</p>
<p><br></p>
<p>despu&eacute;s de la &uacute;ltima actividad en 22 horas</p>
<p>Publicitarias</p>
<p><br></p>
<p>Propiedad Cookie Finalidad Plazo</p>
<p>autonomosyemprende</p>
<p>dor.es _fbp</p>
<p><br></p>
<p>Utilizado por Facebook para ofrecer una serie de productos</p>
<p>tales como publicidad, ofertas en tiempo real de anunciantes</p>
<p>terceros</p>
<p><br></p>
<p>en 3 meses</p>
<p><br></p>
<p>autonomosyemprende</p>
<p>dor.es _gcl_au</p>
<p><br></p>
<p>Utilizado por Google AdSense para experimentar con la</p>
<p>publicidad a trav&eacute;s de la eficiencia de sitios web que utilizan</p>
<p>sus servicios.</p>
<p><br></p>
<p>en 3 meses</p>
<p><br></p>
<p>COOKIES DE TERCEROS</p>
<p><br></p>
<p>Los servicios de terceros son ajenos al control del editor. Los proveedores pueden modificar en todo momento sus</p>
<p>condiciones de servicio, finalidad y utilizaci&oacute;n de las cookies, etc.</p>
<p>Proveedores externos de este sitio web:</p>
<p><br></p>
<p>Editor Pol&iacute;tica de privacidad</p>
<p>Facebook <a data-fr-linked="true" href="https://www.facebook.com/about/privacy/">https://www.facebook.com/about/privacy/</a></p>
<p>Google Analytics <a data-fr-linked="true" href="https://privacy.google.com/take-control.html">https://privacy.google.com/take-control.html</a></p>
<p>Google <a data-fr-linked="true" href="https://privacy.google.com/take-control.html">https://privacy.google.com/take-control.html</a></p>
<p><br></p>
<p>PANEL DE CONFIGURACI&Oacute;N DE COOKIES</p>
<p><br></p>
<p>Desde este panel podr&aacute; configurar las cookies que el sitio web puede instalar en su navegador, excepto las cookies</p>
<p>t&eacute;cnicas o funcionales que son necesarias para la navegaci&oacute;n y la utilizaci&oacute;n de las diferentes opciones o servicios que se</p>
<p>ofrecen.</p>
<p><br></p>
<p>Panel de cookies</p>
<p><br></p>
<p>C&Oacute;MO GESTIONAR LAS COOKIES DESDE EL NAVEGADOR</p>
<p><br></p>
<p>Eliminar las cookies del</p>
<p>dispositivo</p>
<p><br></p>
<p>Las cookies que ya est&aacute;n en un dispositivo se pueden eliminar borrando el historial del</p>
<p>navegador, con lo que se suprimen las cookies de todos los sitios web visitados.</p>
<p>Sin embargo, tambi&eacute;n se puede perder parte de la informaci&oacute;n guardada (por ejemplo,</p>
<p>los datos de inicio de sesi&oacute;n o las preferencias de sitio web).</p>
<p><br></p>
<p>Gestionar las cookies espec&iacute;ficas</p>
<p>del sitio</p>
<p><br></p>
<p>Para tener un control m&aacute;s preciso de las cookies espec&iacute;ficas de cada sitio, los usuarios</p>
<p>pueden ajustar su configuraci&oacute;n de privacidad y cookies en el navegador.</p>
<p><br></p>
<p>Bloquear las cookies</p>
<p><br></p>
<p>Aunque la mayor&iacute;a de los navegadores modernos se pueden configurar para evitar que</p>
<p>se instalen cookies en los dispositivos, eso puede obligar al ajuste manual de</p>
<p>determinadas preferencias cada vez que se visite un sitio o p&aacute;gina. Adem&aacute;s, algunos</p>
<p>servicios y caracter&iacute;sticas pueden no funcionar correctamente (por ejemplo, los inicios de</p>
<p>sesi&oacute;n con perfil).</p>
<p><br></p>
<p>C&Oacute;MO ELIMINAR LAS COOKIES DE LOS NAVEGADORES M&Aacute;S COMUNES</p>
<p><br></p>
<p>Chrome <a data-fr-linked="true" href="http://support.google.com/chrome/answer/95647?hl=es">http://support.google.com/chrome/answer/95647?hl=es</a></p>
<p>Edge <a data-fr-linked="true" href="https://support.microsoft.com/es-es/microsoft-edge/eliminar-las-cookies-en-microsoft-edge-63947406-40ac-c3b8-57b9-2a946a29ae09">https://support.microsoft.com/es-es/microsoft-edge/eliminar-las-cookies-en-microsoft-edge-63947406-40ac-c3b8-57b9-2a946a29ae09</a></p>
<p>Explorer <a data-fr-linked="true" href="https://support.microsoft.com/es-es/help/278835/how-to-delete-cookie-files-in-internet-explorer">https://support.microsoft.com/es-es/help/278835/how-to-delete-cookie-files-in-internet-explorer</a></p>
<p>Firefox <a data-fr-linked="true" href="https://www.mozilla.org/es-ES/privacy/websites/#cookies">https://www.mozilla.org/es-ES/privacy/websites/#cookies</a></p>
<p>Safari <a data-fr-linked="true" href="https://support.apple.com/es-es/guide/safari/sfri11471/mac">https://support.apple.com/es-es/guide/safari/sfri11471/mac</a></p>
<p>Opera <a data-fr-linked="true" href="https://help.opera.com/en/latest/security-and-privacy/#clearBrowsingData">https://help.opera.com/en/latest/security-and-privacy/#clearBrowsingData</a></p>`}} />
          </blockquote>
        </div>
      </div>
      <Footer />
    </div>
  );
}
