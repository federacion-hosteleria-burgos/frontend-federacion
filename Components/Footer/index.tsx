import React, { useState } from "react";
import styles from "./index.module.css";
import Link from "next/link";
import Image from "next/image";
import UnirmeForm from "../UnirmeForm";
import { message } from "antd";

export default function Footer() {
  const [visible, setVisible] = useState(false);

  return (
    <div className={styles.footer_container}>
      <div className={styles.footer_container_items}>
        <div>
          <Image src="/image/logo-federacion.png" width="220" height="140" />
        </div>
        <div>
          <h2>Hostelería de Burgos</h2>
          <ul>
            <li>
              <Link href="/nosotros">
                <a>Cónocenos</a>
              </Link>
            </li>
            <li>
              <Link href="/">
                <a>Asociaciones</a>
              </Link>
            </li>
            <li>
              <Link href="/nosotros">
                <a>Transparencia</a>
              </Link>
            </li>
            <li>
              <a onClick={() => setVisible(true)}>Únete a la Federación</a>
            </li>
            <li>
              <Link href="/noticias">
                <a>Noticias</a>
              </Link>
            </li>
          </ul>
        </div>
        <div>
          <h2>Ayuda y contacto</h2>
          <ul>
            <li>
              <Link href="/contacto">
                <a>Contacto</a>
              </Link>
            </li>
            <li>
              <Link href="/politica-de-privacidad">
                <a>Política de privacidad</a>
              </Link>
            </li>
            <li>
              <Link href="/aviso-legal">
                <a>Aviso legal</a>
              </Link>
            </li>
            <li>
              <Link href="/politica-de-cookies">
                <a>Política de cookies</a>
              </Link>
            </li>
            <li>
              <Link href="/preguntas-frecuentes">
                <a>Preguntas frecuentes</a>
              </Link>
            </li>
          </ul>
        </div>
        <div className={styles.new_lester_content}>
          <h2>Newsletter</h2>
          <form
            className={styles.new_lester}
            onSubmit={(e) =>
             { e.preventDefault();
              message.success("Gracias por unirte a la Federación")}
            }
          >
            <input type="email" placeholder="Introduce tu email" required />
            <button>Enviar</button>
          </form>
          <p>
            Suscríbete a nuestra newsletter y estáras informado con las últimas
            noticias del sector
          </p>
        </div>
      </div>

      <div className={styles.credit_footer}>
        <p>
          Copyright © {new Date().getFullYear()} All rights reserved |
          Federación de Hostelería de Burgos
        </p>
      </div>
      <UnirmeForm isModalVisible={visible} setIsModalVisible={setVisible} />
    </div>
  );
}
