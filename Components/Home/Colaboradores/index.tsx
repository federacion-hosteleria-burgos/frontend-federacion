import React from "react";
import styles from "./index.module.css";

export default function Colaboradores() {
  return (
    <div className={styles.colaboradoras_content}>
      <h1>Entidades colaboradoras</h1>
      <img src="/image/colab.jpg" alt="Colaborador" />
    </div>
  );
}
