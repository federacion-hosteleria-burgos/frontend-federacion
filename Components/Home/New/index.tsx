import React from "react";
import styles from "./index.module.css";
import data from "./data.json";
import Card from "../../CardNew/card";
import Link from "next/link";
import { newQuery } from "../../../GraphQL";
import { useQuery } from "@apollo/client";

export default function News() {
  const { data = {}, loading } = useQuery(newQuery.GET_POST_PARAM, {
    variables: { input: { type: false, event: false }, page: 1, limit: 2 },
  });

  const { getPostParam } = data;

  const posts = getPostParam && getPostParam.data ? getPostParam.data : [];

  const renderItem = (item, i) => {
    return <Card data={item} feed={false} key={i} />;
  };

  return (
    <div className={styles.news_content}>
      <div className={styles.news_content_items}>
        <div className={styles.news_content_item_info}>
          <div>
            <h1>Actualidad del sector</h1>
            <p>Lee las noticias de actialidad del sector de la hostelería</p>
            <Link href="/noticias">Ver todos</Link>
          </div>
        </div>
        <div className={styles.news_content_items_new}>
          {posts &&
            posts.map((item, i) => {
              return renderItem(item, i);
            })}
        </div>
      </div>
    </div>
  );
}
