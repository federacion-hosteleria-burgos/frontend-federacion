import React from "react";
import styles from "./card.module.css";
import Link from "next/link";

export default function Card({ data }) {
  return (
    <div className={styles.card}>
      <div className={styles.card_imagen}>
        <img src={data.image} alt={data.title} />
      </div>
      <div className={styles.card_info}>
        <h2>{data.title}</h2>
        <p>{data.description}</p>
        <Link href={data.link}>
          <a target="_black">Saber más</a>
        </Link>
      </div>
    </div>
  );
}
