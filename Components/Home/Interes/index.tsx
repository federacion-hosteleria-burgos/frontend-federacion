import React from "react";
import styles from "./index.module.css";
import data from "./data.json";
import Card from "./card";
import { newQuery } from "../../../GraphQL";
import { useQuery } from "@apollo/client";

export default function Interes() {
  const { data = {}, loading } = useQuery(newQuery.GET_POST_PARAM, {
    variables: { input: { type: true, event: false }, page: 1, limit: 4 },
  });

  const { getPostParam } = data;

  const posts = getPostParam && getPostParam.data ? getPostParam.data : [];

  const renderItem = (item, i) => {
    return <Card data={item} key={i} />;
  };

  if (loading) {
    return null;
  }
  return (
    <div className={styles.interes_content}>
      <div className={styles.interes_title}>
        <h1>¡¡Te interesa!!</h1>
        <p>
          Descubre los mejores tips para inpulsar tu negocio con los consejos de
          los mejores expertos del sector{" "}
        </p>
      </div>

      <div className={styles.interes_card}>
        {posts &&
          posts.map((item, i) => {
            return renderItem(item, i);
          })}
      </div>
    </div>
  );
}
