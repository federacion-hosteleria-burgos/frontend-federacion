import React from "react";
import Hero from "./Hero";
import Footer from "../Footer";
import Adds from "./Ads";
import Ventajas from "./Ventajas";
import PartOne from "./Part1";
import Interes from "./Interes";
import News from "./New";
import Colaboradores from "./Colaboradores";

export default function Home() {
  return (
    <div>
      <Hero />
      <Adds />
      <Ventajas />
      <PartOne />
      <Interes />
      <News />
      <Colaboradores />
      <Footer />
    </div>
  );
}
