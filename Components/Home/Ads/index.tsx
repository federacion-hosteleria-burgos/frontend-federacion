import React from "react";
import styles from "./index.module.css";
import WithStyles from "./Card";
import { useQuery, useMutation } from "@apollo/client";
import { newQuery, newMutation } from "../../../GraphQL";

export default function Anuncios() {
  const { data, refetch, loading } = useQuery(newQuery.GET_RANDOM_ADS);

  const ads = data && data.getAds ? data.getAds.data : [];

  return (
    <div className={styles.container_ads}>
      <div className={styles.slider}>
        <div className={styles.slide_track}>
          {ads.map((ad, i) => {
            return (
              <WithStyles
                name={ad.name}
                image={ad.image}
                click={ad.click}
                id={ad._id}
                url={ad.url}
                key={i}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}
