import React from "react";
import styles from "./index.module.css";
import { useMutation } from "@apollo/client";
import { newMutation } from "../../../GraphQL";

export default function WithStyles({ name, image, click, id, url }) {
  const [updateAds] = useMutation(newMutation.UPDATE_ADS_CLICK);

  const updateView = (click: number, id: string) => {
    updateAds({ variables: { data: { _id: id, click: click } } })
      .then((res) => {})
      .catch((e) => {
        console.log(e);
      });
  };

  const onPress = () => {
    updateView(click + 1, id);
    window.open(url, "_blank");
  };

  return (
    <div
      onClick={onPress}
      className={styles.slide}
      style={{
        backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.4)), url(${image})`,
      }}
    >
      <div>
        <p>{name}</p>
        <h3>Patrocinador</h3>
      </div>
    </div>
  );
}
