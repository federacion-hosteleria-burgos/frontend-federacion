import React from "react";
import styles from "./index.module.css";

export default function Ventajas() {
  return (
    <div className={styles.content_ventajas}>
      <h1>Conoce las ventajas de estar asociado</h1>
      <h4>
        Busque nuestra Asociación provincial más cercana, póngase en contacto
        con ella y descubra las ventajas de estar asociado. Estos son algunos
        ejemplos:{" "}
      </h4>
      <div className={styles.items_content}>
        <div className={styles.items}>
          <img src="/image/work.png" />
          <h2>ASESORÍA</h2>
          <p>
            Tramitación de recursos, alegaciones, pliegos de descargo en todo
            tipo de expediente sancionadores, motivados por: Problemas de
            ruidos, horarios de cierre y acceso a menores. Inspecciones de
            Turismo, Sanidad, etc. Reclamaciones de clientes. Denuncias y
            seguimiento del intrusismo y competencia desleal. Reclamaciones y
            gestiones de cobro de clientes morosos.
          </p>
        </div>
        <div className={styles.items}>
          <img src="/image/bar.png" />
          <h2>ASESORÍA LABORAL</h2>
          <p>
            Altas de empresa, libros de inspección y registro de personal.
            Confección de contratos, altas y bajas en la Seguridad Social de los
            trabajadores y autónomos, eleboración de nóminas y seguros sociales,
            liquidación de finiquitos, retenciones y resúmenes anuales.
            Sanciones, despidos y expedientes de regulación de empleo. Recursos
            e Inspecciones de trabajo. Personación en actos de conciliación y
            defensa ante el juzgado de los Social. Gestión de pensiones de
            jubilación, bajas por incapacidad laboral y convenio especial.
            Tramitación de solicitud de subvenciones a la creación de empleo,
            etc.
          </p>
        </div>
        <div className={styles.items}>
          <img src="/image/certified.png" />
          <h2>ASESORÍA FISCAL</h2>
          <p>
            Altas, Bajas y modificaciones del Impuesto de Actividades económicas
            y Censo de Actividades. Constitución de Sociedades Civiles y
            Comunidades de Bienes. Liquidación de las retenciones por alquiler
            de bienes inmuebles. Declaración del Impuesto de la Renta de las
            Personas Físicas y del Patrimonio. Solicitud y transmisión del
            Permiso Municipal de Apertura y Licencia de Actividad. Asesoramiento
            y recursos ante la Administración Tributaria.
          </p>
        </div>
        <div className={styles.items}>
          <img src="/image/click.png" />
          <h2>SERVICIOS GENERALES</h2>
          <p>
            Asesoramiento y elaboración de contratos de arrendamiento,
            actualización de rentas, traspasos, desahucios.... Cursos y
            reuniones profesionales Cursos de reciclaje y formación continua de
            empresas Seguros de accidentes para los trabajadores en condiciones
            ventajosas Compra agrupada de la energía. Circulares informativas
            sobre novedades de interés. Servicios de documentación, información,
            difusión, propaganda, etc. Gestión y planificación de actividades.
          </p>
        </div>
      </div>
    </div>
  );
}
