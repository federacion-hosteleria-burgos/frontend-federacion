import React from "react";
import styles from "./index.module.css";

export default function PartOne() {
  return (
    <div className={styles.partOne_content}>
      <div className={styles.partOne_bg_imagen}>
        <img src="/image/Rectangle.png" alt="Part One" />
      </div>
      <div className={styles.partOne_bg}>
        <div className={styles.partOne_bg_text}>
          <h2>Conocimiento sectorial</h2>
          <p>
            HOSTELERÍA DE BURGOS a través de su Departamento de Estudios pone a
            disposición la información referente a la evolución del sector de la
            hostelería y el turismo, a través de una serie de Informes de
            Hostelería de distinta periodicidad, que son accesibles mediante una
            suscripción que permite el acceso a los mismos durante un período de
            un año.
          </p>
          <button>Saber más</button>
        </div>
      </div>
    </div>
  );
}
