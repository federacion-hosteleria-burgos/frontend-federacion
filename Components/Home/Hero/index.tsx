import React, { useState } from "react";
import styles from "./index.module.css";
import NavBar from "../../NavBar";
import UnirmeForm from "../../UnirmeForm";

export default function Hero() {
  const [visible, setVisible] = useState(false);
  return (
    <div className={styles.hero_container}>
      <NavBar home={true} />
      <div className={styles.hero_container_text}>
        <h1>Federación de Hostelería de Burgos</h1>
        <p>
          Bienvenidos al portal de la Federación de Hostelería de Burgos
          enterate de las noticial del sector.
        </p>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <button onClick={() => setVisible(true)}>Únirme</button>
          <a href="/nosotros">Conocenos</a>
        </div>
      </div>

      <UnirmeForm isModalVisible={visible} setIsModalVisible={setVisible} />
    </div>
  );
}
