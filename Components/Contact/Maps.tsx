import GoogleMapReact from "google-map-react";

export default function Maps(props) {
  const { height, width, lat, lgn, title } = props;
  const mapObj = {
    center: {
      lat: Number(lat) || 40.4378698,
      lng: Number(lgn) || -3.8196207,
    },
    city: "Burgos",
  };

  const renderMarkers = (map, maps) => {
    new maps.Marker({
      position: mapObj.center,
      map,
      title: title,
    });
  };

  return (
    <div style={{ height: height, width: width }}>
      <GoogleMapReact
        yesIWantToUseGoogleMapApiInternals={true}
        bootstrapURLKeys={{ key: "AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA" }}
        center={mapObj.center}
        defaultZoom={16}
        onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
      />
    </div>
  );
}
