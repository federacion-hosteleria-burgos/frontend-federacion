import React from "react";
import styles from "./index.module.css";
import NavBar from "../../NavBar";

export default function Hero() {
  return (
    <div className={styles.hero_container}>
      <NavBar home={true} />
      <div className={styles.hero_container_text}>
        <h1>¿Podemos ayudarte?</h1>
        <p>{`Inicio -> Contacto`}</p>
      </div>
    </div>
  );
}
