import React from "react";
import styles from "./index.module.css";
import Footer from "../Footer";
import { HomeOutlined, PhoneOutlined, MailOutlined } from "@ant-design/icons";
import Hero from "./Hero";
import { Form, Input, Button, Checkbox, message } from "antd";
import Maps from "./Maps";
import { MAIN_URL } from "../../utils/Urls";

const { TextArea } = Input;

export default function contact() {
  const [form] = Form.useForm();
  const onFinish = async (valuess: any) => {
    let res = await fetch(`${MAIN_URL}/contact`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(valuess),
    });
    if (res) {
      const user = await res.json();
      if (user.success) {
        message.success(user.messages);
        form.resetFields();
      } else {
        message.warning(user.messages);
      }
    } else {
      message.error("Algo salio mal intentalo de nuevo");
    }
  };

  return (
    <div>
      <Hero />
      <div className={styles.contact_container}>
        <div className={styles.contact_container_form}>
          <div>
            <h1>SEDE</h1>

            <div className={styles.items}>
              <HomeOutlined style={{ fontSize: 32, color: "#b37b02" }} />
              <div style={{ marginLeft: 15 }}>
                <p>Burgos, España</p>
                <p>Plaza de Castilla 1, 2o. 09003</p>
              </div>
            </div>

            <div className={styles.items}>
              <PhoneOutlined style={{ fontSize: 32, color: "#b37b02" }} />
              <div style={{ marginLeft: 15 }}>
                <p>+91 352 91 56</p>
                <p>L - J: 8:30 a 18:00h.</p>
                <p>V: 8:30 a 14:00h.</p>
              </div>
            </div>

            <div className={styles.items}>
              <MailOutlined style={{ fontSize: 32, color: "#b37b02" }} />
              <div style={{ marginLeft: 15 }}>
                <a
                  href="mailto:info@federacionhosteleriaburgos.es"
                  target="_black"
                >
                  info@federacionhosteleriaburgos.es
                </a>
                <p>¡Envíenos su consulta en cualquier momento!</p>
              </div>
            </div>
          </div>

          <div>
            <Form
              name="contact"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              form={form}
              style={{
                margin: 20,
                justifyContent: "center",
                alignItems: "center",
              }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              autoComplete="off"
            >
              <Form.Item
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Por favor intruduce un correo electrónico!",
                  },
                ]}
              >
                <Input placeholder="Correo electrónico" />
              </Form.Item>

              <Form.Item
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Por favor intruduce un número teléfono!",
                  },
                ]}
              >
                <Input placeholder="Teléfono" />
              </Form.Item>

              <Form.Item
                name="name"
                rules={[
                  { required: true, message: "Por favor intruduce un nombre!" },
                ]}
              >
                <Input placeholder="Nombre" />
              </Form.Item>

              <Form.Item
                name="lastName"
                rules={[
                  {
                    required: true,
                    message: "Por favor intruduce un apellido!",
                  },
                ]}
              >
                <Input placeholder="Apellidos" />
              </Form.Item>

              <Form.Item
                name="message"
                rules={[
                  {
                    required: true,
                    message: "Por favor intruduce un mensaje!",
                  },
                ]}
              >
                <TextArea rows={4} placeholder="Mensaje" maxLength={100} />
              </Form.Item>

              <Form.Item
                name="policy"
                valuePropName="checked"
                rules={[
                  {
                    required: true,
                    message:
                      "Por favor acepte las política de protección de datos de la web!",
                  },
                ]}
              >
                <Checkbox>
                  Si haces clic en «Enviar», confirmas haber leído y aceptado la{" "}
                  <a>Política de protección de datos de la web</a>.
                </Checkbox>
              </Form.Item>

              <Button
                type="primary"
                htmlType="submit"
                style={{ height: 40, borderRadius: 100 }}
              >
                Enviar mensaje
              </Button>
            </Form>
          </div>
        </div>
        <div style={{ marginTop: 100, marginBottom: 100 }}>
          <Maps
            height={500}
            width="100%"
            lat="42.33795"
            lgn="-3.7085754"
            title="Federación de Hostelería de Burgos"
          />
        </div>
      </div>
      <Footer />
    </div>
  );
}
