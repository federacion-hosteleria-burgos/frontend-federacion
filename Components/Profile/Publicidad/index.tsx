import { useState } from "react";
import {
  Table,
  Tag,
  Space,
  Button,
  Popover,
  Select,
  message,
  Popconfirm,
} from "antd";
import styles from "./index.module.css";
import { PlusCircleOutlined, EyeOutlined } from "@ant-design/icons";
import { useQuery, useMutation } from "@apollo/client";
import { newQuery, newMutation } from "../../../GraphQL";
import moment from "moment";
import ModalAddEdit from "./ModalEditAdd";

const { Option } = Select;

const content = (
  <div>
    <p>Click de usuario en el anuncio</p>
  </div>
);

export default function Publicidad() {
  const [visible, setVisible] = useState(false);
  const [visibleAdd, setVisibleAdd] = useState(false);
  const [dataAds, setDataads] = useState(null);

  const [eliminarAds] = useMutation(newMutation.DELETED_ADS);
  const { data, refetch, loading } = useQuery(newQuery.GET_RANDOM_ADS);

  const ads = data && data.getAds ? data.getAds.data : [];

  const openModal = (datas: any) => {
    setDataads(datas);
    setVisible(true);
  };

  const deletedAds = (id: string) => {
    eliminarAds({ variables: { id: id } })
      .then((res) => {
        if (res.data.eliminarAds.success) {
          refetch();
          message.success("Anuncio eliminado con exito");
          setDataads(null);
        } else {
          refetch();
          message.warning("Algo salio mal intentalo de nuevo");
        }
      })
      .catch((e) => {
        console.log(e);
        message.error("Algo salio mal intentalo de nuevo");
      });
  };

  const columns = [
    {
      title: "Imagen",
      dataIndex: "image",
      key: "image",
      render: (image: any) => (
        <div className={styles.imagen}>
          <img src={image} alt="Ads Wilbby" />
        </div>
      ),
    },
    {
      title: "Nombre",
      dataIndex: "name",
      key: "name",
      render: (text: any) => <p>{text}</p>,
    },
    {
      title: "Fecha inicio",
      key: "created_at",
      render: (datas: any) => (
        <p>
          {moment(datas.created_at)
            .utcOffset(120)
            .add(45, "minute")
            .format("lll")}
        </p>
      ),
    },
    {
      title: "Fecha de fin",
      key: "end_date",
      render: (datas: any) => (
        <p>
          {moment(datas.end_date)
            .utcOffset(120)
            .add(45, "minute")
            .format("lll")}
        </p>
      ),
    },
    {
      title: "Clicks",
      dataIndex: "click",
      render: (tags: any) => (
        <Popover content={content} title="Clicks del anuncio">
          <span>
            <EyeOutlined /> {tags}
          </span>
        </Popover>
      ),
    },
    {
      title: "Acción",
      key: "action",
      render: (data: any) => (
        <Space size="middle">
          <Button type="link" onClick={() => openModal(data)}>
            Editar
          </Button>
          <Popconfirm
            title="¿Estas seguro que deseas eliminar este anuncio?"
            onConfirm={() => deletedAds(data._id)}
            onCancel={() => {}}
            okText="Eliminar"
            cancelText="Cancelar"
          >
            <Button type="link" danger>
              Eliminar
            </Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <div className={styles.My_datos}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginBottom: 40,
          marginTop: 20,
        }}
      >
        <Button
          icon={<PlusCircleOutlined />}
          type="primary"
          onClick={() => setVisibleAdd(true)}
          style={{borderRadius: 100}}
        >
          Añadir banner
        </Button>
      </div>
      <Table columns={columns} dataSource={ads} loading={loading} />
      {dataAds ? (
        <ModalAddEdit
          visible={visible}
          setVisible={setVisible}
          refetch={refetch}
          data={dataAds}
          setDataads={setDataads}
        />
      ) : null}

      <ModalAddEdit
        visible={visibleAdd}
        setVisible={setVisibleAdd}
        refetch={refetch}
        data={null}
        setDataads={setDataads}
      />
    </div>
  );
}
