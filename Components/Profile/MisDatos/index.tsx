import React, { useState } from "react";
import styles from "./index.module.css";
import { Input, Button, Avatar, Tooltip, message } from "antd";
import { useMutation } from "@apollo/client";
import { newMutation } from "../../../GraphQL";

export default function MisDatos({ user, refetch }: any) {
  const [name, setname] = useState(user.name);
  const [lastName, setlastName] = useState(user.lastName);
  const [email, setemail] = useState(user.email);
  const [phone, setphone] = useState(user.phone);
  const [avatar, setAvatar] = useState(user.avatar);
  const [bar, setbar] = useState(user.adress ? user.adress.bar : "");
  const [city, setcity] = useState(user.city);
  const [adress, setadress] = useState(user.adress ? user.adress.adress : "");
  const [Loading, setLoadin] = useState(false);

  const [actualizarUsuario] = useMutation(newMutation.ACTUALIZAR_USUARIO);

  const actualizarUsuarioData = () => {
    setLoadin(true);
    const input = {
      _id: user._id,
      name: name,
      lastName: lastName,
      email: email,
      phone: phone,
      city: city,
      adress: {
        bar: bar,
        adress: adress,
      },
    };
    actualizarUsuario({ variables: { input: input } })
      .then((res: any) => {
        if (res.data.actualizarUsuario.success) {
          refetch();
          message.success(res.data.actualizarUsuario.message);
          setLoadin(false);
        } else {
          refetch();
          message.warning(res.data.actualizarUsuario.message);
          setLoadin(false);
        }
      })
      .catch(() => {
        refetch();
        message.error("Algo salio mal intentalo de nuevo");
      })
      .finally(() => {
        setLoadin(false);
      });
  };

  return (
    <div className={styles.My_datos}>
      <div className={styles.My_datos_form}>
        <div className={styles.item_content}>
          <div
            className={styles.item_content_title}
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <h2>Detalles De La Cuenta</h2>
            <div style={{ marginBottom: 20 }}>
              <Tooltip title="Cambiar tu foto del perfil">
                <Avatar src={avatar} size={75} />
              </Tooltip>
            </div>
          </div>
          <div className={styles.item_content_content}>
            <div>
              <p>¿Cuál Es Tu Nombre?</p>
              <Input
                placeholder="Nombre"
                className={styles.custon_input}
                value={name}
                onChange={(e) => setname(e.target.value)}
                style={{ height: 50 }}
              />
            </div>

            <div style={{ marginTop: 20 }}>
              <p>¿Cuales Son Tus Apellidos?</p>
              <Input
                placeholder="Apellidos"
                className={styles.custon_input}
                value={lastName}
                onChange={(e) => setlastName(e.target.value)}
                style={{ height: 50 }}
              />
            </div>

            <div style={{ marginTop: 20 }}>
              <p>¿Cuál es tu localidad?</p>
              <Input
                placeholder="Localidad"
                className={styles.custon_input}
                value={city}
                onChange={(e) => setcity(e.target.value)}
                style={{ height: 50 }}
              />
            </div>

            <div style={{ marginTop: 20 }}>
              <div style={{ display: "flex" }}>
                <div style={{ marginRight: 6, width: "50%" }}>
                  <p>Email</p>
                  <Input
                    placeholder="Email"
                    className={styles.custon_input}
                    value={email}
                    onChange={(e) => setemail(e.target.value)}
                    style={{ height: 50 }}
                  />
                </div>
                <div style={{ marginLeft: 6, width: "50%" }}>
                  <p>Teléfono Móvil</p>
                  <Input
                    placeholder="Email"
                    className={styles.custon_input}
                    value={phone}
                    onChange={(e) => setphone(e.target.value)}
                    style={{ height: 50 }}
                  />
                </div>
              </div>
            </div>

            <div style={{ marginTop: 20 }}>
              <div style={{ display: "flex" }}>
                <div style={{ marginRight: 6, width: "50%" }}>
                  <p>Establecimiento</p>
                  <Input
                    placeholder="Nombre del establecimiento"
                    className={styles.custon_input}
                    value={bar}
                    onChange={(e) => setbar(e.target.value)}
                    style={{ height: 50 }}
                  />
                </div>
                <div style={{ marginLeft: 6, width: "50%" }}>
                  <p>Dirección</p>
                  <Input
                    placeholder="Dirección del establecimiento"
                    className={styles.custon_input}
                    value={adress}
                    onChange={(e) => setadress(e.target.value)}
                    style={{ height: 50 }}
                  />
                </div>
              </div>
            </div>

            <div>
              <Button
                type="primary"
                className={styles.form_button}
                loading={Loading}
                onClick={actualizarUsuarioData}
                style={{
                  height: 50,
                  width: "100%",
                  marginTop: 30,
                  borderRadius: 100,
                }}
              >
                Guardar cambios
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
