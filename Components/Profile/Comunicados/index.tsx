import React, { useState } from "react";
import styles from "./index.module.css";
import { Result, Segmented, Button } from "antd";
import CreateComunicado from "./CrearComunicados";
import { useQuery } from "@apollo/client";
import { newQuery } from "../../../GraphQL";
import Item from "./Items";
import Loading from "../../Loading";

export default function Proveedores({ user }: any) {
  const [visible, setVisible] = useState(false);
  const [type, setType] = useState("Comunicados de la federación");

  const {
    data = {},
    loading,
    refetch,
  } = useQuery(newQuery.GET_COMUNICADO, { variables: { type: type } });

  const { getComunicados } = data;

  const comunicado =
    getComunicados && getComunicados.data ? getComunicados.data : [];

  const renderItem = (item: any, i: number) => {
    return <Item item={item} refetch={refetch} user={user} key={i} />;
  };

  const isAdmin = user.isAdmin
    ? [
        "Comunicados de la federación",
        "Notas informativa",
        "Asamblea",
        "Junta directiva",
      ]
    : ["Comunicados de la federación", "Notas informativa", "Asamblea"];

  const options = isAdmin;

  return (
    <div className={styles.My_datos}>
      <div className={styles.My_datos_form}>
        {user.isAdmin ? (
          <div
            style={{
              marginTop: 30,
              marginBottom: 30,
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <Button
              type="primary"
              style={{ height: 40, borderRadius: 100 }}
              onClick={() => setVisible(true)}
            >
              Crear comunicado
            </Button>
          </div>
        ) : null}
        <div
          style={{
            marginTop: 30,
            justifyContent: "center",
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
          }}
        >
          <Segmented
            value={type}
            options={options}
            onChange={(value: any) => setType(value)}
          />
          {comunicado && comunicado.length > 0 ? (
            <div style={{ marginTop: 50 }}>
              <>
                {loading ? (
                  <Loading />
                ) : (
                  <>
                    {comunicado &&
                      comunicado.map((item: any, i: number) => {
                        return renderItem(item, i);
                      })}
                  </>
                )}
              </>
            </div>
          ) : (
            <Result
              status="404"
              title="Aún no hay comunicados de la federación"
              subTitle="Lo sentimos pero aún no tenemos comunicados de la federación."
              style={{ marginTop: 50 }}
            />
          )}
        </div>
      </div>
      <CreateComunicado
        visible={visible}
        setVisible={setVisible}
        user={user}
        refetch={refetch}
      />
    </div>
  );
}
