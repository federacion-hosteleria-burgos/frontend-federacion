import React from "react";
import { newQuery } from "../../../../GraphQL";
import { useQuery } from "@apollo/client";
import Footer from "../../../Footer";
import NavBar from "../../../NavBar";
import styles from "./index.module.css";
import Head from "next/head";
import moment from "moment";

export default function Detalles({ id }) {
  const {
    data = {},
    loading,
    refetch,
  } = useQuery(newQuery.GET_COMUNICADO_ID, { variables: { id: id } });

  const { getComunicadosID } = data;

  const post =
    getComunicadosID && getComunicadosID.data ? getComunicadosID.data : null;

  if (loading) {
    return null;
  }

  return (
    <div>
      <Head>
        <title>{post.title}</title>
        <meta name="description" content={post.title} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar home={false} />
      <div className={styles.feed_container}>
        <div className={styles.feed_container_header}>
          <h1>{post.title}</h1>
          <span>
            {post.user.name} {post.user.name} ·{" "}
            {`${moment(post.created_at).fromNow()} | ${moment(
              post.created_at
            ).format("LLL")}`}
          </span>
          <div
            dangerouslySetInnerHTML={{ __html: post.content }}
            className={styles.details}
          />
        </div>
      </div>
      <Footer />
    </div>
  );
}
