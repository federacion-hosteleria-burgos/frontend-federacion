import React, { useState } from "react";
import { Modal, Input, Button, Select, message } from "antd";
import { modules, formats } from "./source";
import dynamic from "next/dynamic";
import { useMutation } from "@apollo/client";
import { newMutation } from "../../../../GraphQL";

const { Option } = Select;

const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });

export default function CreateComunicado({
  visible,
  setVisible,
  user,
  refetch,
}: any) {
  const [value, setValue] = useState("");
  const [title, setTitle] = useState("");
  const [type, setType] = useState("Comunicados de la federación");
  const [Loading, setLoading] = useState(false);

  const [crearCommunicado] = useMutation(newMutation.CREAR_COMUNICADO);

  function onChange(value: string) {
    setType(value);
  }

  const isOK = () => {
    if (title && value) {
      return false;
    } else {
      return true;
    }
  };

  const crearCommunicadoData = () => {
    setLoading(true);
    const input = {
      title: title,
      type: type,
      content: value,
      user: user,
    };

    crearCommunicado({ variables: { input: input } })
      .then((res) => {
        if (res.data.crearCommunicado.success) {
          message.success(res.data.crearCommunicado.message);
          refetch();
          setLoading(false);
          setVisible(false);
        } else {
          message.warning(res.data.crearCommunicado.message);
          setLoading(false);
          refetch();
        }
      })
      .catch(() => {
        message.success("Algo salio mal intentalo de nuevo");
        setLoading(false);
      });
  };

  return (
    <Modal
      title="Crear comunicado"
      visible={visible}
      footer={null}
      width={800}
      onCancel={() => setVisible(false)}
    >
      <div>
        <Input
          placeholder="Titulo del comunicado"
          style={{ marginBottom: 20 }}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <Select
          style={{ width: "100%", marginBottom: 30 }}
          showSearch
          defaultValue={type}
          placeholder="Seleccionar un tipo de comunicado"
          optionFilterProp="children"
          onChange={onChange}
          filterOption={(input, option) =>
            //@ts-ignore
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          <Option value="Comunicados de la federación">
            Comunicados de la federación
          </Option>
          <Option value="Notas informativa">Notas informativa</Option>
          <Option value="Asamblea">Asamblea</Option>
          <Option value="Junta directiva">Junta directiva</Option>
        </Select>

        <ReactQuill
          theme="snow"
          value={value}
          onChange={setValue}
          modules={modules}
          formats={formats}
        />
        <Button
          type="primary"
          loading={Loading}
          style={{
            borderRadius: 100,
            height: 45,
            width: "100%",
            marginTop: 50,
          }}
          disabled={isOK()}
          onClick={crearCommunicadoData}
        >
          Publica comunicado
        </Button>
      </div>
    </Modal>
  );
}
