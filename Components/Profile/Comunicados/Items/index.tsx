import React from "react";
import styles from "./index.module.css";
import moment from "moment";
import { Button, Modal, message } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/client";
import { newMutation } from "../../../../GraphQL";
import Link from "next/link";

const { confirm } = Modal;

export default function Item({ item, refetch, user }) {
  const [eliminarComunication] = useMutation(newMutation.ELIMINAR_COMUNICADO);

  const eliminarComunicationData = () => {
    eliminarComunication({ variables: { id: item._id } })
      .then((res) => {
        if (res.data.eliminarComunication.success) {
          message.success(res.data.eliminarComunication.messages);
          refetch();
        } else {
          message.warning(res.data.eliminarComunication.messages);
          refetch();
        }
      })
      .catch(() => {
        message.error("Algo salio mal intentalo de nuevo");
      });
  };
  function showConfirm() {
    confirm({
      title: "¿Estás seguro que deseas eliminar este comunicado?",
      icon: <ExclamationCircleOutlined />,
      content: "Los comunicado eliminado no se podrán recuperar",
      okText: "Eliminar",
      okType: "danger",
      cancelText: "Cancelar",
      onOk() {
        eliminarComunicationData();
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  return (
    <Link href={`/comunicado/${item._id}`}>
      <a target="_black">
        <div className={styles.item}>
          <div>
            <h1>{item.title}</h1>
          </div>
          <div>
            <span>
              {item.user.name} {item.user.name} ·{" "}
              {`${moment(item.created_at).fromNow()} | ${moment(
                item.created_at
              ).format("LLL")}`}
            </span>
            {user.isAdmin ? (
              <Button
                type="link"
                danger
                style={{ marginLeft: "auto" }}
                onClick={showConfirm}
              >
                Eliminar
              </Button>
            ) : null}
          </div>
        </div>
      </a>
    </Link>
  );
}
