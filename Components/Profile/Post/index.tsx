import React, { useState } from "react";
import { Table, Space, Button, message, Popconfirm } from "antd";
import { useQuery, useMutation } from "@apollo/client";
import { newQuery, newMutation } from "../../../GraphQL";
import styles from "./index.module.css";
import CratePost from "../CreatePost";

const { Column, ColumnGroup } = Table;

export default function Proveedores({ user }) {
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(20);
  const [visible, setVisible] = useState(false);

  const [eliminarPost] = useMutation(newMutation.DELETE_POST);

  const {
    data = {},
    loading,
    refetch,
  } = useQuery(newQuery.GET_POST_PARAM, {
    variables: { input: {}, page: page, limit: limit },
  });

  const { getPostParam } = data;

  const posts = getPostParam && getPostParam.data ? getPostParam.data : [];

  const deletedPost = (id) => {
    eliminarPost({ variables: { id: id } })
      .then((res) => {
        if (res.data.eliminarPost.success) {
          message.success(res.data.eliminarPost.messages);
          refetch();
        } else {
          message.warning(res.data.eliminarPost.messages);
        }
      })
      .catch(() => {
        message.success("Algo salio mal intentalo de nuevo");
      });
  };

  const getTipo = (s) => {
    if (s.type) {
      return "Blog";
    } else if (s.event) {
      return "Evento";
    } else if (s.revista) {
      return "Revista";
    } else {
      return "Noticia";
    }
  };

  return (
    <div className={styles.My_datos}>
      <div className={styles.header}>
        <div>
          {user.isAdmin ? (
            <Button
              type="primary"
              style={{ marginTop: 20, borderRadius: 100 }}
              onClick={() => setVisible(true)}
            >
              Crear entrada
            </Button>
          ) : null}
        </div>
      </div>
      <Table
        dataSource={posts}
        loading={loading}
        scroll={{ x: "calc(100vh - 4em)" }}
        pagination={{
          defaultCurrent: page,
          defaultPageSize: limit,
          total: 1,
          onChange: (page, pageSize) => {
            setPage(page);
            setLimit(pageSize);
          },
        }}
      >
        <ColumnGroup title="Info">
          <Column
            title="Nombre del post"
            key="marca"
            render={(datas) => {
              return (
                <div className={styles.marca}>
                  <img src={datas.image} alt={datas.title} />
                  <div>
                    <p style={{ margin: 0 }}>{datas.title}</p>
                  </div>
                </div>
              );
            }}
          />
        </ColumnGroup>
        <Column
          title="Tipo"
          key="location"
          render={(datas) => {
            return (
              <div>
                <p>{getTipo(datas)}</p>
              </div>
            );
          }}
        />
        <Column
          title="Acciones"
          key="action"
          render={(datas: any) => (
            <Space size="middle">
              <Popconfirm
                title="¿Estás seguro que deseas eliminar esta entrada?"
                onConfirm={() => deletedPost(datas._id)}
                onCancel={() => {}}
                okText="Eliminar"
                cancelText="Cancelar"
              >
                <Button type="link" danger>
                  Eliminar
                </Button>
              </Popconfirm>
            </Space>
          )}
        />
      </Table>
      {user ? (
        <CratePost
          isModalVisible={visible}
          setIsModalVisible={setVisible}
          user={user}
          refetch={refetch}
        />
      ) : null}
    </div>
  );
}
