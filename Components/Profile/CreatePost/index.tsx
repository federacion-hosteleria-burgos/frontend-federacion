import React, { useState } from "react";
import {
  Form,
  Input,
  Button,
  message,
  Modal,
  Switch,
  Tooltip,
  Upload,
} from "antd";
import { newMutation } from "../../../GraphQL";
import { useMutation } from "@apollo/client";
import { PlusCircleOutlined, LoadingOutlined } from "@ant-design/icons";
import styles from "./index.module.css";

const { TextArea } = Input;

function getBase64(file: any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

export default function CreatePost({
  isModalVisible,
  setIsModalVisible,
  user,
  refetch,
}: any) {
  const [form] = Form.useForm();
  const [blog, setBlog] = useState(false);
  const [event, setEvent] = useState(false);
  const [revista, setRevista] = useState(false);
  const [imagen, setImagen] = useState(null);
  const [loadingImage, setloadingImage] = useState(false);

  const [crearPost] = useMutation(newMutation.CREAR_POST);
  const [singleUploadToStoreImagenAws] = useMutation(
    newMutation.UPLOAD_FILE_STORE
  );

  const onFinish = (values: any) => {
    const input = {
      title: values.title,
      type: blog,
      image: values.imagen ? values.imagen : imagen,
      description: values.description,
      link: values.link,
      user: user,
      revista: revista,
      event: event,
    };
    crearPost({ variables: { input: input } })
      .then((res) => {
        if (res.data.crearPost.success) {
          message.success(res.data.crearPost.message);
          refetch();
          setTimeout(() => {
            setIsModalVisible(false);
            form.resetFields();
          }, 500);
        } else {
          refetch();
          message.warning(res.data.crearPost.message);
        }
      })
      .catch(() => {
        message.error("Algo salio mal intentalo de nuevo");
      });
  };

  const uploadButton = (
    <Tooltip title="Añadir foto de la noticia">
      <div
        style={{
          width: 450,
          height: 150,
          borderRadius: 15,
          alignItems: "center",
          justifyContent: "center",
          padding: 10,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {loadingImage ? (
          <LoadingOutlined style={{ fontSize: 30, color: "#b37b02" }} />
        ) : (
          <PlusCircleOutlined style={{ fontSize: 30, color: "#b37b02" }} />
        )}

        <div className={styles.ant_upload_text}>
          <span style={{ fontSize: 12 }}>Añadir foto de la noticia</span>
        </div>
      </div>
    </Tooltip>
  );

  return (
    <Modal
      title="Crear noticia o blog"
      visible={isModalVisible}
      footer={null}
      onCancel={() => setIsModalVisible(false)}
    >
      <div>
        <div
          style={{
            width: 450,
            height: 200,
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: 0,
          }}
        >
          <Upload
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            customRequest={async (data) => {
              setloadingImage(true);
              let file = await getBase64(data.file);
              singleUploadToStoreImagenAws({
                variables: { file },
              })
                .then((res: any) => {
                  setloadingImage(false);
                  setImagen(
                    res.data.singleUploadToStoreImagenAws.data.Location
                  );
                })
                .catch((error: any) => {
                  setloadingImage(false);
                  message.error(
                    "Imagen muy grande reduce el tamaño de la misma"
                  );
                });
            }}
          >
            {imagen ? (
              <Tooltip title="Haz click para cambiar">
                <img className={styles.imagen_prod_add} src={imagen} />
              </Tooltip>
            ) : null}

            {!imagen ? uploadButton : null}
          </Upload>
        </div>

        <p>
          Si añades una URL de imagen no añada una imagen desde tu ordenador y
          viceversa
        </p>

        <Form
          name="contact"
          form={form}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            name="imagen"
            rules={[
              {
                required: false,
                message: "Por favor intruduce una URL de imagen!",
              },
            ]}
          >
            <Input placeholder="URL imagen de la noticia (Opcioanl)" />
          </Form.Item>
          <Form.Item
            name="title"
            rules={[
              { required: true, message: "Por favor intruduce un titulo!" },
            ]}
          >
            <Input placeholder="Titulo" />
          </Form.Item>

          <Form.Item
            name="description"
            rules={[
              {
                required: false,
                message: "Por favor intruduce una descripcion!",
              },
            ]}
          >
            <TextArea rows={4} placeholder="Descripcion corta" />
          </Form.Item>
          <Form.Item
            name="link"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un enlace de la noticia!",
              },
            ]}
          >
            <Input placeholder="Enlace de la noticia" />
          </Form.Item>

          <Switch
            checkedChildren="Blog"
            unCheckedChildren="Noticia"
            defaultChecked={blog}
            onChange={(checked) => setBlog(checked)}
            style={{ marginBottom: 20 }}
          />

          <Switch
            checkedChildren="Evento"
            unCheckedChildren="Evento"
            defaultChecked={event}
            onChange={(checked) => setEvent(checked)}
            style={{ marginBottom: 20, marginLeft: 10 }}
          />

          <Switch
            checkedChildren="Revista"
            unCheckedChildren="Revista"
            defaultChecked={revista}
            onChange={(checked) => setRevista(checked)}
            style={{ marginBottom: 20, marginLeft: 10 }}
          />
          <br />

          <Button
            type="primary"
            htmlType="submit"
            style={{ height: 40, borderRadius: 100 }}
          >
            Crear entrada
          </Button>
        </Form>
      </div>
    </Modal>
  );
}
