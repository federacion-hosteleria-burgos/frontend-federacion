import React, { useState, useEffect } from "react";
import Footer from "../Footer";
import { Button, Modal } from "antd";
import styles from "./topnav.module.css";
import NavBar from "../NavBar";
import {
  AuditOutlined,
  UserOutlined,
  AlignLeftOutlined,
  CloseOutlined,
  AppstoreAddOutlined,
  ContactsOutlined,
  UserAddOutlined,
  LogoutOutlined,
  ExclamationCircleOutlined,
  PlayCircleOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import MiDatos from "./MisDatos";
import Proveedores from "./Proveedores";
import Comunicados from "./Comunicados";
import Miembros from "./Miembros";
import { useQuery } from "@apollo/client";
import { newQuery } from "../../GraphQL";
import LoadingComp from "../Loading";
import Publicidad from "./Publicidad";
import Post from "./Post";

const { confirm } = Modal;

export default function Profile() {
  const [active_index, setActive_index] = useState(1);
  const [menuShown, setMenuShown] = useState(false);
  const [windowSize, setWindowSize] = useState({
    width: 0,
    height: 0,
  });

  const { data = {}, loading, refetch } = useQuery(newQuery.GET_USER);

  const { getUsuario } = data;

  const user = getUsuario && getUsuario.data ? getUsuario.data : null;

  const router = useRouter();

  const changeTab = (index) => {
    setActive_index(index);
  };

  const toggleMenuMobile = () => {
    if (windowSize.width < 640) {
      setMenuShown(!menuShown);
    }
  };

  useEffect(() => {
    // only execute all the code below in client side
    if (typeof window !== "undefined") {
      // Handler to call on window resize
      function handleResize() {
        // Set window width/height to state
        setWindowSize({
          width: window.innerWidth,
          height: window.innerHeight,
        });
      }

      // Add event listener
      window.addEventListener("resize", handleResize);

      // Call handler right away so state gets updated with initial window size
      handleResize();

      // Remove event listener on cleanup
      return () => window.removeEventListener("resize", handleResize);
    }
  }, []);

  function showConfirm() {
    confirm({
      title: "¿Estás seguro que deseas cerrar sesión?",
      icon: <ExclamationCircleOutlined />,
      content: "Esperamos verte pronto por aquí",
      okText: "Cerrar sesión",
      okType: "danger",
      cancelText: "Cancelar",
      onOk() {
        window.localStorage.removeItem("token");
        window.localStorage.removeItem("id");
        setTimeout(() => {
          router.reload();
        }, 1000);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  if (loading) {
    return <LoadingComp />;
  }

  return (
    <div>
      <NavBar home={false} />
      <div className={styles.main_wrapper}>
        <div
          className={`${styles.topnav} ${
            menuShown ? styles.visible_mobile : ""
          }`}
        >
          <div className={styles.toggle_nav} onClick={toggleMenuMobile}>
            {menuShown ? (
              <CloseOutlined
                style={{ fontSize: 28, color: "#b37b02", paddingTop: 5 }}
              />
            ) : (
              <AlignLeftOutlined
                style={{ fontSize: 28, color: "#b37b02", paddingTop: 5 }}
              />
            )}
          </div>
          <div className={styles.nav_items} onClick={toggleMenuMobile}>
            <Button
              className={active_index === 1 ? styles.active : styles.simple}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
              onClick={() => changeTab(1)}
            >
              <UserOutlined style={{ fontSize: 28 }} />{" "}
              <h3 className={styles.primary}> MIS DATOS</h3>
            </Button>
            <Button
              className={active_index === 2 ? styles.active : styles.simple}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
              onClick={() => changeTab(2)}
            >
              <ContactsOutlined style={{ fontSize: 28 }} />{" "}
              <h3 className={styles.primary}>PROVEEDORES</h3>
            </Button>

            <Button
              className={active_index === 3 ? styles.active : styles.simple}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
              onClick={() => changeTab(3)}
            >
              <AuditOutlined style={{ fontSize: 28 }} />{" "}
              <h3 className={styles.primary}>COMUNICADOS</h3>
            </Button>

            {user.isAdmin ? (
              <Button
                className={active_index === 80 ? styles.active : styles.simple}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
                onClick={() => changeTab(80)}
              >
                <AppstoreAddOutlined style={{ fontSize: 28 }} />{" "}
                <h3 className={styles.primary}>CREAR POST</h3>
              </Button>
            ) : null}

            {user.isAdmin ? (
              <Button
                className={active_index === 90 ? styles.active : styles.simple}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
                onClick={() => changeTab(90)}
              >
                <PlayCircleOutlined style={{ fontSize: 28 }} />{" "}
                <h3 className={styles.primary}>PUBLICIDAD</h3>
              </Button>
            ) : null}

            {user.isAdmin ? (
              <Button
                className={active_index === 5 ? styles.active : styles.simple}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
                onClick={() => changeTab(5)}
              >
                <UserAddOutlined style={{ fontSize: 28 }} />{" "}
                <h3 className={styles.primary}>CREAR MIEMBRO</h3>
              </Button>
            ) : null}

            <Button
              className={active_index === 6 ? styles.active : styles.simple}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
              onClick={showConfirm}
            >
              <LogoutOutlined style={{ fontSize: 28, color: "red" }} />{" "}
              <h3 className={styles.primary} style={{ color: "red" }}>
                CERRAR SESIÓN
              </h3>
            </Button>
          </div>
        </div>
        <div className={styles.page_content}>
          {active_index === 1 && user ? (
            <MiDatos user={user} refetch={refetch} />
          ) : null}
          {active_index === 2 && user ? <Proveedores user={user} /> : null}
          {active_index === 3 && user ? <Comunicados user={user} /> : null}
          {active_index === 5 && user ? <Miembros user={user} /> : null}
          {active_index === 90 && user ? <Publicidad /> : null}
          {active_index === 80 && user ? <Post user={user} /> : null}
        </div>
      </div>

      <Footer />
    </div>
  );
}
