import React, { useState } from "react";
import { Form, Input, Button, message, Modal, Switch } from "antd";
import { newMutation } from "../../../../GraphQL";
import { useMutation } from "@apollo/client";
import { onCreateUser } from "../../../Login/funtion";

export default function UnirmeForm(props: any) {
  const [form] = Form.useForm();
  const { isModalVisible, setIsModalVisible, user, refetch } = props;
  const [isAdmin, setisAdmin] = useState(false);

  const [crearUsuario] = useMutation(newMutation.CREAR_USUARIO);

  const onFinish = (values: any) => {
    const input = {
      name: values.name,
      lastName: values.lastName,
      email: values.email,
      password: "admin",
      phone: values.phone,
      city: values.city,
      nie: values.dni,
      adress: {
        bar: values.store,
        adress: values.adress,
      },
      isAdmin: isAdmin,
      createdBy: user._id,
      iban: "",
    };

    crearUsuario({ variables: { input: input } })
      .then((res) => {
        if (res.data.crearUsuario.success) {
          onCreateUser(values.email);
          message.success(res.data.crearUsuario.message);
          refetch();
          setTimeout(() => {
            setIsModalVisible(false);
            form.resetFields();
          }, 1000);
        } else {
          refetch();
          message.warning(res.data.crearUsuario.message);
        }
      })
      .catch(() => {
        message.error("Algo salio mal intentalo de nuevo");
      });
  };

  return (
    <Modal
      title="Crear miembro"
      visible={isModalVisible}
      footer={null}
      onCancel={() => setIsModalVisible(false)}
    >
      <div style={{ margin: 20 }}>
        <Form
          name="contact"
          form={form}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            name="name"
            rules={[
              { required: true, message: "Por favor intruduce un nombre!" },
            ]}
          >
            <Input placeholder="Nombre" />
          </Form.Item>

          <Form.Item
            name="lastName"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un apellido!",
              },
            ]}
          >
            <Input placeholder="Apellidos" />
          </Form.Item>

          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un correo electrónico!",
              },
            ]}
          >
            <Input placeholder="Correo electrónico" />
          </Form.Item>

          <Form.Item
            name="city"
            rules={[
              {
                required: true,
                message: "Por favor intruduce una localidad!",
              },
            ]}
          >
            <Input placeholder="Localidad" />
          </Form.Item>

          <Form.Item
            name="dni"
            rules={[
              {
                required: false,
                message: "Por favor intruduce un DNI!",
              },
            ]}
          >
            <Input placeholder="DNI" />
          </Form.Item>

          <Form.Item
            name="phone"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un teléfono!",
              },
            ]}
          >
            <Input placeholder="Teléfono" />
          </Form.Item>

          <Form.Item
            name="store"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un nombre del establecimiento!",
              },
            ]}
          >
            <Input placeholder="Nombre del establecimiento" />
          </Form.Item>

          <Form.Item
            name="adress"
            rules={[
              {
                required: true,
                message:
                  "Por favor intruduce una dirección del establecimiento!",
              },
            ]}
          >
            <Input placeholder="Dirección del establecimiento" />
          </Form.Item>

          <Switch
            checkedChildren="Administrador"
            unCheckedChildren="Miembro"
            defaultChecked={isAdmin}
            onChange={(checked) => setisAdmin(checked)}
            style={{ marginBottom: 20 }}
          />
          <br />

          <Button
            type="primary"
            htmlType="submit"
            style={{ height: 40, borderRadius: 100 }}
          >
            Crear miembro
          </Button>
        </Form>
      </div>
    </Modal>
  );
}
