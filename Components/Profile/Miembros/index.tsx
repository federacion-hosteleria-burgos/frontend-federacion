import React, { useState } from "react";
import {
  Table,
  Tag,
  Button,
  Statistic,
  Input,
  Space,
  message,
  Popconfirm,
} from "antd";
import { useQuery, useMutation } from "@apollo/client";
import { newQuery, newMutation } from "../../../GraphQL";
import moment from "moment";
import styles from "./index.module.css";
import { PhoneOutlined, AudioOutlined } from "@ant-design/icons";
import CrateMember from "./UnirmeForm";

const { Search } = Input;

export default function Miembros({ user }: any) {
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(20);
  const [search, setSearch] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [eliminarUsuario] = useMutation(newMutation.DELETE_USER);

  const {
    data = {},
    loading,
    refetch,
  } = useQuery(newQuery.GET_USER_ALL, {
    variables: { search: search, page: page, limit: limit },
  });

  const { getUserAdmin } = data;

  const count = getUserAdmin ? getUserAdmin.count : 0;

  const orders = getUserAdmin ? getUserAdmin.data : [];

  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: "#b37b02",
      }}
    />
  );

  const updateUserData = (id: string) => {
    eliminarUsuario({ variables: { id: id } })
      .then((res) => {
        if (res.data.eliminarUsuario.success) {
          message.success(res.data.eliminarUsuario.messages);
          refetch();
        } else {
          message.warning(res.data.eliminarUsuario.messages);
          refetch();
        }
      })
      .catch(() => {
        message.warning("Algo salio mal intentalo de nuevo");
      });
  };

  const columns = [
    {
      title: "Nombre y apellido",
      key: "name",
      render: (datas: any) => {
        return (
          <div className={styles.header_avatar}>
            <img src={datas.avatar} alt={datas.name} />

            <div style={{ marginLeft: 10 }}>
              {datas.adress ? (
                <h3
                  style={{ margin: 0, fontWeight: "700" }}
                >{`${datas.adress.bar}`}</h3>
              ) : null}

              <p>{`${datas.name} ${datas.lastName}`}</p>
              {datas.adress ? (
                <span
                  style={{ color: "gray" }}
                >{`${datas.adress.adress}`}</span>
              ) : null}
            </div>
          </div>
        );
      },
    },
    {
      title: "Teléfono y Email",
      key: "phone",
      render: (datas: any) => {
        return (
          <div>
            <Button type="link" icon={<PhoneOutlined />}>
              {datas.phone ? datas.phone : "No phone"}
            </Button>
            <br />
            <span style={{ color: "gray" }}>{`${datas.email}`}</span>
          </div>
        );
      },
    },

    {
      title: "Localidad",
      key: "from",
      render: (datas: any) => {
        return (
          <div>
            <p>{datas.city}</p>
          </div>
        );
      },
    },
    {
      title: "Adminitrado",
      key: "status",
      render: (datas: any) => {
        return (
          <div>
            {datas.isAdmin ? (
              <Tag color="green">Administrador</Tag>
            ) : (
              <Tag color="red">Miembro</Tag>
            )}
          </div>
        );
      },
    },
    {
      title: "Detalles",
      key: "details",
      render: (datas: any) => {
        return (
          <Space size="middle">
            <Popconfirm
              title="¿Estás seguro que deseas eliminar este miembro?"
              onConfirm={() => updateUserData(datas._id)}
              onCancel={() => {}}
              okText="Eliminar"
              cancelText="Cancelar"
            >
              <Button type="link" danger>
                Eliminar
              </Button>
            </Popconfirm>
          </Space>
        );
      },
    },
  ];

  return (
    <div className={styles.My_datos}>
      <div className={styles.header}>
        <div>
          <Statistic title="Total de miembros" value={count} />
          <Button
            type="primary"
            style={{ marginTop: 20, borderRadius: 100 }}
            onClick={() => setIsModalVisible(true)}
          >
            Crear miembro
          </Button>
        </div>
        <Search
          placeholder="Buscar miembros"
          enterButton="Buscar"
          size="middle"
          allowClear
          suffix={suffix}
          style={{ width: 400, borderRadius: 100 }}
          onSearch={(value) => setSearch(value)}
        />
      </div>
      <Table
        columns={columns}
        dataSource={orders}
        loading={loading}
        scroll={{ x: "calc(100vh - 4em)" }}
        pagination={{
          defaultCurrent: page,
          defaultPageSize: limit,
          total: count,
          onChange: (page, pageSize) => {
            setPage(page);
            setLimit(pageSize);
          },
        }}
      />
      <CrateMember
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        user={user}
        refetch={refetch}
      />
    </div>
  );
}
