import React from "react";
import styles from "./index.module.css";

export default function Status({ data }: any) {
  return (
    <div className={styles.description}>
      <h2>Vendedores</h2>
      {data.vendedores.map((vendedore) => {
        return (
          <div style={{ marginTop: 30 }}>
            <h2>
              {vendedore.name} {vendedore.lastName}
            </h2>
            <p>{vendedore.phone}</p>
            <p>{vendedore.email}</p>
            <p>{vendedore.city}</p>
          </div>
        );
      })}
    </div>
  );
}
