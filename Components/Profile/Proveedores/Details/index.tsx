import React from "react";
import { Modal, Button } from "antd";
import ImagenView from "./imagenView";
import TitleAndPrice from "./TitleAndPrice";
import Description from "./Description";
import Representante from "./representante";

export default function Details({
  data,
  modalIsOpen,
  setIsOpen,
  setDataModal,
}: any) {
  return (
    <Modal
      title={null}
      visible={modalIsOpen}
      footer={null}
      onCancel={() => {
        setIsOpen(false);
        setDataModal(null);
      }}
    >
      <ImagenView data={data} />
      <TitleAndPrice data={data} />
      <Description data={data} />
      <Button
        style={{ borderRadius: 100, width: "100%", marginTop: 20 }}
        type="primary"
        href={data.url}
        target="_black"
      >
        Ver catálogo del proveedor
      </Button>
      <Representante data={data} />
    </Modal>
  );
}
