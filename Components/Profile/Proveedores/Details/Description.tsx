import React from "react";
import styles from "./index.module.css";

export default function Description({ data }: any) {
  return (
    <div className={styles.description}>
      <h2>Descripción</h2>
      <p>{data.description}</p>
    </div>
  );
}
