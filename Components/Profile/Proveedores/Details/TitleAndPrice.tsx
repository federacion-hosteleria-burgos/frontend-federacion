import React from "react";
import styles from "./index.module.css";

export default function TitleAndPrice({ data }: any) {
  return (
    <div className={styles.title}>
      <div>
        <h2>{`${data.name} ${data.mark}`}</h2>
      </div>
    </div>
  );
}
