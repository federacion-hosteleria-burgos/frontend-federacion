import React from "react";
import styles from "./index.module.css";

export default function ImagenView({ data }: any) {
  return (
    <div style={{ margin: 15 }}>
      <img src={data.imagen} alt={data.name} className={styles.images} />
    </div>
  );
}
