import React, { useState } from "react";
import styles from "./index.module.css";
import {
  Modal,
  Input,
  Form,
  Tooltip,
  Button,
  Upload,
  message,
  Space,
} from "antd";
import { newMutation } from "../../../../GraphQL";
import { useMutation } from "@apollo/client";
import {
  PlusCircleOutlined,
  LoadingOutlined,
  MinusCircleOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import Tags from "./tag";

const { TextArea } = Input;

function getBase64(file: any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

export default function Add(props: any) {
  const [form] = Form.useForm();
  const { isModalVisible, setIsModalVisible, user, refetch } = props;

  const [imagen, setImagen] = useState(null);
  const [loadingImage, setloadingImage] = useState(false);
  const [tags, setTags] = useState<string[]>([]);

  const [singleUploadToStoreImagenAws] = useMutation(
    newMutation.UPLOAD_FILE_STORE
  );

  const [createProveedor] = useMutation(newMutation.CREATED_PROVEEDOR);

  const uploadButton = (
    <Tooltip title="Añadir foto de banner">
      <div
        style={{
          width: 450,
          height: 150,
          borderRadius: 15,
          alignItems: "center",
          justifyContent: "center",
          padding: 10,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {loadingImage ? (
          <LoadingOutlined style={{ fontSize: 30, color: "#b37b02" }} />
        ) : (
          <PlusCircleOutlined style={{ fontSize: 30, color: "#b37b02" }} />
        )}

        <div className={styles.ant_upload_text}>
          <span style={{ fontSize: 12 }}>Añadir foto de banner</span>
        </div>
      </div>
    </Tooltip>
  );

  const createdProveedor = (values: any) => {
    const input = {
      name: values.name,
      imagen: imagen,
      description: values.description,
      mark: values.mark,
      products: tags,
      phone: values.phone,
      city: values.city,
      cede: values.cede,
      vendedores: values.vendedores,
      user: user._id,
      url: values.url,
    };

    if (!imagen) {
      message.warning("Debes añadir una imagen");
    } else if (tags.length === 0) {
      message.warning("Debes añadir productor de referencia");
    } else {
      createProveedor({ variables: { input: input } })
        .then((res) => {
          if (res.data.createProveedor.success) {
            message.success(res.data.createProveedor.messages);
            refetch();
            setTimeout(() => {
              setIsModalVisible(false);
              form.resetFields();
              setImagen(null);
              setTags([]);
            }, 500);
          } else {
            refetch();
            message.warning(res.data.createProveedor.messages);
          }
        })
        .catch((e) => {
          console.log(e);
          message.error("Algo salio mal intentalo de nuevo");
        });
    }
  };
  const onFinish = (values: any) => {
    createdProveedor(values);
  };

  return (
    <Modal
      title="Crear proveedor"
      visible={isModalVisible}
      width={800}
      footer={null}
      onCancel={() => {
        setIsModalVisible(false);
      }}
    >
      <div
        style={{
          width: 450,
          height: 200,
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: 0,
          marginBottom: 20,
        }}
      >
        <Upload
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          customRequest={async (data) => {
            setloadingImage(true);
            let file = await getBase64(data.file);
            singleUploadToStoreImagenAws({
              variables: { file },
            })
              .then((res: any) => {
                setloadingImage(false);
                setImagen(res.data.singleUploadToStoreImagenAws.data.Location);
              })
              .catch((error: any) => {
                setloadingImage(false);
                message.error("Imagen muy grande reduce el tamaño de la misma");
              });
          }}
        >
          {imagen ? (
            <Tooltip title="Haz click para cambiar">
              <img className={styles.imagen_prod_add} src={imagen} />
            </Tooltip>
          ) : null}

          {!imagen ? uploadButton : null}
        </Upload>

        <div
          style={{
            textAlign: "center",
            marginTop: 10,
          }}
        >
          <Button
            type="primary"
            href="https://www.iloveimg.com/es/comprimir-imagen"
            target="_blank"
          >
            Comprimir imagen
          </Button>
        </div>
      </div>
      <Form name="contact" form={form} onFinish={onFinish} autoComplete="off">
        <Form.Item
          name="name"
          rules={[{ required: true, message: "Campo obligatorío!" }]}
        >
          <Input placeholder="Nombre" />
        </Form.Item>
        <Form.Item
          name="mark"
          rules={[{ required: true, message: "Campo obligatorío!" }]}
        >
          <Input placeholder="Marca comercial" />
        </Form.Item>

        <Form.Item
          name="phone"
          rules={[{ required: true, message: "Campo obligatorío!" }]}
        >
          <Input placeholder="Teléfono" />
        </Form.Item>
        <Form.Item
          name="city"
          rules={[{ required: true, message: "Campo obligatorío!" }]}
        >
          <Input placeholder="Ciudad o Cede" />
        </Form.Item>
        <Form.Item
          name="cede"
          rules={[{ required: true, message: "Campo obligatorío!" }]}
        >
          <Input placeholder="Dirección" />
        </Form.Item>
        <Form.Item
          name="url"
          rules={[{ required: true, message: "Campo obligatorío!" }]}
        >
          <Input placeholder="URL de catálogo del proveedor" />
        </Form.Item>
        <div style={{ marginTop: 10, marginBottom: 20 }}>
          <Tags tags={tags} setTags={setTags} title="Añadir productos" />
        </div>
        <Form.Item
          name="description"
          rules={[
            {
              required: false,
              message: "Campo obligatorío!",
            },
          ]}
        >
          <TextArea
            rows={4}
            placeholder="Descripción del proveedor"
            maxLength={100}
          />
        </Form.Item>

        <h2>Añadir vendedores</h2>

        <Form.List name="vendedores">
          {(fields, { add, remove }) => (
            <>
              {fields.map(({ key, name, ...restField }) => (
                <Space
                  key={key}
                  style={{ display: "flex", marginBottom: 8 }}
                  align="baseline"
                >
                  <Form.Item
                    {...restField}
                    name={[name, "name"]}
                    rules={[{ required: true, message: "Campo obligatorío" }]}
                  >
                    <Input placeholder="Nombre" />
                  </Form.Item>
                  <Form.Item
                    {...restField}
                    name={[name, "email"]}
                    rules={[{ required: true, message: "Campo obligatorío" }]}
                  >
                    <Input placeholder="Email" />
                  </Form.Item>
                  <Form.Item
                    {...restField}
                    name={[name, "phone"]}
                    rules={[{ required: true, message: "Campo obligatorío" }]}
                  >
                    <Input placeholder="Teléfono" />
                  </Form.Item>
                  <Form.Item
                    {...restField}
                    name={[name, "city"]}
                    rules={[{ required: true, message: "Campo obligatorío" }]}
                  >
                    <Input placeholder="Ciudades (separado en ,)" />
                  </Form.Item>
                  <MinusCircleOutlined onClick={() => remove(name)} />
                </Space>
              ))}
              <Form.Item>
                <Button
                  type="dashed"
                  onClick={() => add()}
                  block
                  icon={<PlusOutlined />}
                >
                  Añadir vendedor a este proveedor
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>

        <Button
          type="primary"
          htmlType="submit"
          style={{ height: 40, borderRadius: 100 }}
        >
          Crear proveedor
        </Button>
      </Form>
    </Modal>
  );
}
