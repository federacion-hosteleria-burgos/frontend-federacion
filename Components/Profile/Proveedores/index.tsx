import React, { useState } from "react";
import {
  Table,
  Tag,
  Space,
  Button,
  message,
  Popconfirm,
  Statistic,
  Input,
} from "antd";
import { useQuery, useMutation } from "@apollo/client";
import { newQuery, newMutation } from "../../../GraphQL";
import styles from "./index.module.css";
import { AudioOutlined } from "@ant-design/icons";
import DetailModal from "./Details";
import Add from "./Ads";
import Editar from "./Edit";

const { Search } = Input;

const { Column, ColumnGroup } = Table;

export default function Proveedores({ user }) {
  const [visible, setVisible] = useState(false);
  const [visibleAds, setVisibleAds] = useState(false);
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [dataModal, setDataModal] = useState(null);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(20);
  const [search, setSearch] = useState("");
  const [dataEdit, setDataEdit] = useState(null);
  const [eliminarProveedor] = useMutation(newMutation.DELETE_PROVEEDOR);

  const {
    data = {},
    loading,
    refetch,
  } = useQuery(newQuery.GET_PROVEEDOR, {
    variables: {
      search: search,
      page: page,
      limit: limit,
    },
  });

  const { getProveedor } = data;

  const count = getProveedor ? getProveedor.count : 0;

  const ads = getProveedor ? getProveedor.data : [];

  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: "#b37b02",
      }}
    />
  );

  const deletedAdsFunc = (id: string) => {
    eliminarProveedor({ variables: { id: id } })
      .then((res) => {
        if (res.data.eliminarProveedor.success) {
          message.success("Proveedor marcado como eliminado");
          refetch();
        } else {
          message.warning("Algo salio mal intentalo de nuevo");
        }
      })
      .catch(() => {
        message.error("Algo salio mal intentalo de nuevo");
      });
  };

  const onpenDetails = (datas: any) => {
    setDataModal(datas);
    setVisible(true);
  };

  const onpenEdits = (datas: any) => {
    setDataEdit(datas);
    setVisibleEdit(true);
  };

  return (
    <div className={styles.My_datos}>
      <div className={styles.header}>
        <div>
          <Statistic title="Total proveedores" value={count} />
          {user.isAdmin ? (
            <Button
              type="primary"
              style={{ marginTop: 20, borderRadius: 100 }}
              onClick={() => setVisibleAds(true)}
            >
              Crear proveedor
            </Button>
          ) : null}
        </div>
        <Search
          placeholder="Buscar proveedor"
          enterButton="Buscar"
          size="middle"
          suffix={suffix}
          allowClear
          style={{ width: 400, borderRadius: 100 }}
          onSearch={(value) => setSearch(value)}
        />
      </div>
      <Table
        dataSource={ads}
        loading={loading}
        scroll={{ x: "calc(100vh - 4em)" }}
        pagination={{
          defaultCurrent: page,
          defaultPageSize: limit,
          total: count,
          onChange: (page, pageSize) => {
            setPage(page);
            setLimit(pageSize);
          },
        }}
      >
        <ColumnGroup title="Info">
          <Column
            title="Proveedor"
            key="marca"
            render={(datas) => {
              return (
                <div className={styles.marca}>
                  <img src={datas.imagen} alt={datas.name} />
                  <div>
                    <p style={{ margin: 0 }}>{datas.name}</p>
                  </div>
                </div>
              );
            }}
          />
          <Column
            title="Marca"
            key="model"
            render={(datas) => {
              return <p>{datas.mark}</p>;
            }}
          />
        </ColumnGroup>
        <Column
          title="Ubicacón"
          key="location"
          render={(datas) => {
            return (
              <div>
                <p>{datas.city}</p>
              </div>
            );
          }}
        />

        <Column
          title="Teléfono"
          key="visible"
          render={(datas) => {
            return (
              <div>
                <p>{datas.phone}</p>
              </div>
            );
          }}
        />
        <Column
          title="Productos"
          dataIndex="products"
          key="products"
          render={(tags) => {
            return (
              <>
                {tags.map((tag) => {
                  return <Tag color="blue">{tag}</Tag>;
                })}
              </>
            );
          }}
        />
        <Column
          title="Acciones"
          key="action"
          render={(datas: any) => (
            <Space size="middle">
              <Button type="link" onClick={() => onpenDetails(datas)}>
                Detalles
              </Button>

              {user.isAdmin ? (
                <Button type="link" onClick={() => onpenEdits(datas)}>
                  Editar
                </Button>
              ) : null}

              {user.isAdmin ? (
                <Popconfirm
                  title="¿Estás seguro que deseas eliminar este anuncio?"
                  onConfirm={() => deletedAdsFunc(datas._id)}
                  onCancel={() => {}}
                  okText="Eliminar"
                  cancelText="Cancelar"
                >
                  <Button type="link" danger>
                    Eliminar
                  </Button>
                </Popconfirm>
              ) : null}
            </Space>
          )}
        />
      </Table>
      {dataModal ? (
        <DetailModal
          data={dataModal}
          modalIsOpen={visible}
          setIsOpen={setVisible}
          setDataModal={setDataModal}
          refetch={refetch}
        />
      ) : null}
      <Add
        isModalVisible={visibleAds}
        setIsModalVisible={setVisibleAds}
        user={user}
        refetch={refetch}
      />

      {dataEdit ? (
        <Editar
          isModalVisible={visibleEdit}
          setIsModalVisible={setVisibleEdit}
          user={user}
          refetch={refetch}
          data={dataEdit}
          setDataEdit={setDataEdit}
        />
      ) : null}
    </div>
  );
}
