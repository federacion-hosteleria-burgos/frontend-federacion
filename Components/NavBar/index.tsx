import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Navbar } from "responsive-navbar-react";

export default function NavBar({ home }) {
  const route = useRouter();

  const [Scroll, setScroll] = useState(0);

  useEffect(() => {
    window.onscroll = function () {
      myFunction();
    };
  }, []);

  function myFunction() {
    setScroll(window.document.documentElement.scrollTop);
  }

  const props = {
    items: [
      {
        text: "Inicio",
        link: "/",
      },
      {
        text: "Nosotros",
        link: "/nosotros",
      },
      {
        text: "Información turistica",
        link: "/turismo",
      },
      {
        text: "Blog / Noticias",
        link: "/noticias",
      },
      {
        text: "Revistas",
        link: "/revista",
      },
      {
        text: "Contacto",
        link: "/contacto",
      },
      {
        text: <button onClick={() => route.push("/login")}>ASOCIADOS</button>,
        link: "/login",
      },
    ],
    float: true,

    logo: {
      text: <img src="/image/logo-federacion.png" />,
      link: "/",
    },
    style: {
      barStyles: {
        background: Scroll > 50 ? "#212121" : home ? "transparent" : "#212121",
        zindex: 1000,
      },
      sidebarStyles: {
        background: Scroll > 50 ? "#212121" : "#212121",
        buttonColor: "white",
      },

      linkStyles: {
        color: home
          ? Scroll > 30
            ? "#b37b02"
            : "white"
          : Scroll > 30
          ? "#b37b02"
          : "white",
        fontSize: 16,
        fontWeight: "400",
      },
    },
  };

  return <Navbar {...props} />;
}
