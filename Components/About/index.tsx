import React from "react";
import styles from "./index.module.css";
import Footer from "../Footer";
import Hero from "./Hero";
import Partner from "../Home/Colaboradores";

export default function About() {
  return (
    <div>
      <Hero />
      <div className={styles.about_container}>
        <div className={styles.about_container_title}>
          <h1>Visión - Misión - Valores</h1>
          <p>
            ASÓCIATE, SÓLO LA UNIÓN Y SOLIDARIDAD DE TODOS CONSEGUIRÁ QUE
            PODAMOS AFRONTAR LA DEFENSA DE NUESTROS INTERESES Y AFRONTAR EL
            FUTURO CON ÉXITO
          </p>
        </div>
        <div className={styles.items} style={{ marginTop: 100 }}>
          <h2>Visión</h2>
          <p>
            Generación de unidad de acción y búsqueda de un compromiso
            compartido.
          </p>
        </div>
        <div className={styles.items}>
          <h2>Misión</h2>
          <p>
            Transparencia, rigor y coherencia en la información y en la
            actuación.
          </p>
        </div>
        <div className={styles.items}>
          <h2>OBJETIVO </h2>
          <p>
            Cercanía y creación de confianza en las relaciones con socios y
            asociados.
          </p>
        </div>

        <div className={styles.about_items}>
          <div>
            <img src="/image/quienes.jpg" alt="Nosotros" />
          </div>
          <div>
            <h2>¿Quienes Somos?</h2>
            <p>
              La Federación Provincial de Hostelería es una organización de
              ámbito provincial que agrupa y representa a una buena parte del
              sector hostelero de Burgos y su provincia, siendo su deseo, contar
              con la participación de todos los empresarios de la provincia.
            </p>
          </div>
        </div>

        <div className={styles.teamContent}>
          <h1>
            JUNTA DIRECTIVA DE LA FEDERACIÓN PROVINCIAL DE EMPRESARIOS DE
            HOSTELERÍA
          </h1>
          <h3>Presidente: Luis Mata Olano (Vermutería Victoria)</h3>
          <h3>Vicepresidente: Luis M. O. (Hotel Norte y Londres)</h3>
          <h1>ASOCIACIÓN BURGALESA DE HOSPEDAJE</h1>
          <h3>Presidente: Luis M. O. (Hotel Norte y Londres)</h3>
          <h3>Vicepresidente: Juan Carlos B.P. (Hotel Corona de Castilla)</h3>
          <h2>Vocales:</h2>
          <p>José Luis L. P. (Hotel Mesón del Cid)</p>
          <p>Javier Y.T. (Hotel Tudanca / Miranda de Ebro)</p>
          <p> Carlos A.D. (Hotel Azofra)</p>
          <p>Roberto A.(Hostal Rimbombín)</p>
          <h1>ASOCIACIÓN BURGALESA DE RESTAURACIÓN </h1>
          <h3>Presidente: Luis Mata Olano</h3>
          <h3>
            Vicepresidente: Enrique M. (Restaurante Coco Atapuerca /
            Cardeñajimeno)
          </h3>
          <h2>Vocales:</h2>
          <p>Fidel L. (La Lorencita)</p>
          <p>Gustavo SC. P. (Bar La Bóveda)</p>
          <p>José María T. (Restaurante Maridaje ́s)</p>
          <p>Alfonso M. (Restaurante Lis 2 / Lerma)</p>
          <p>Isabel Á. (Restaurante Maricastaña)</p>
          <p>Pedro Ángel A. (Bar San Pablo)</p>
          <p>Enrique S. (El Soportal)</p>
          <h1>Asociación Burgalesa de Bares especiales y ocio nocturno</h1>
          <h3>Presidente: Juan Antonio Ll. N. (Discoteca Sohho)</h3>
          <h3>Vicepresidente: Rubén H. (Bar Qué Thomas)</h3>
          <h1>ASOCIACIÓN BURGOS SALE</h1>
          <h3> Presidenta: Mariam H.</h3>
          <h1>Asociación de Hostelería de Las Merindades (AHOMER)</h1>
          <h3>Presidente: Juan Francisco G. S.</h3>
          <h1>
            ARANDA DE DUERO Asociación de Hosteleros de Aranda y la Ribera
            (ASOHAR)
          </h1>
          <h3>Presidenta: Nuria Leal</h3>
          <h1>
            MIRANDA DE EBRO Asociación de HostelerosALTAMIRA. MIRANDA DE EBRO
          </h1>
          <h3>Presidente: José R.</h3>
          <h1>Asociación de hostelería de Merindades</h1>
          <h3>Presidente: Juan S.</h3>
        </div>

        <Partner />
      </div>
      <Footer />
    </div>
  );
}
