import React from "react";
import { Modal } from "antd";
import { Form, Input, Button, Checkbox, message } from "antd";
import { MAIN_URL } from "../../utils/Urls";

const { TextArea } = Input;

export default function UnirmeForm(props: any) {
  const [form] = Form.useForm();
  const { isModalVisible, setIsModalVisible } = props;

  const onFinish = async (values: any) => {
    let res = await fetch(`${MAIN_URL}/add-member`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(values),
    });
    if (res) {
      const user = await res.json();
      if (user.success) {
        message.success(user.messages);
        setIsModalVisible(false);
        form.resetFields();
      } else {
        message.warning(user.messages);
      }
    } else {
      message.error("Algo salio mal intentalo de nuevo");
    }
  };

  return (
    <Modal
      title="Unirme a la federación"
      visible={isModalVisible}
      footer={null}
      onCancel={() => setIsModalVisible(false)}
    >
      <div style={{ margin: 20 }}>
        <Form
          name="contact"
          form={form}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            name="name"
            rules={[
              { required: true, message: "Por favor intruduce un nombre!" },
            ]}
          >
            <Input placeholder="Nombre" />
          </Form.Item>

          <Form.Item
            name="lastName"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un apellido!",
              },
            ]}
          >
            <Input placeholder="Apellidos" />
          </Form.Item>

          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un correo electrónico!",
              },
            ]}
          >
            <Input placeholder="Correo electrónico" />
          </Form.Item>

          <Form.Item
            name="phone"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un teléfono!",
              },
            ]}
          >
            <Input placeholder="Teléfono" />
          </Form.Item>

          <Form.Item
            name="store"
            rules={[
              {
                required: true,
                message: "Por favor intruduce un nombre del restaurante o bar!",
              },
            ]}
          >
            <Input placeholder="Nombre del restaurante o bar" />
          </Form.Item>

          <Form.Item
            name="adress"
            rules={[
              {
                required: true,
                message:
                  "Por favor intruduce una dirección del establecimiento!",
              },
            ]}
          >
            <Input placeholder="Dirección del establecimiento" />
          </Form.Item>

          <Form.Item
            name="message"
            rules={[
              {
                required: false,
                message: "Por favor intruduce un mensaje!",
              },
            ]}
          >
            <TextArea rows={4} placeholder="Mensaje" maxLength={100} />
          </Form.Item>

          <Form.Item
            name="policy"
            valuePropName="checked"
            rules={[
              {
                required: true,
                message:
                  "Por favor acepte las política de protección de datos de la web!",
              },
            ]}
          >
            <Checkbox>
              Si haces clic en «Enviar», confirmas haber leído y aceptado la{" "}
              <a>Política de protección de datos de la web</a>.
            </Checkbox>
          </Form.Item>

          <Button
            type="primary"
            htmlType="submit"
            style={{ height: 40, borderRadius: 100 }}
          >
            Enviar mensaje
          </Button>
        </Form>
      </div>
    </Modal>
  );
}
