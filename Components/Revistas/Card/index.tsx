import React from "react";
import styles from "./index.module.css";
import Link from "next/link";

export default function Card({ data }) {
  return (
    <Link href={data.link}>
      <a target="_black" className={styles.card_news_vertical}>
        <img src={data.image} alt={data.title} className={styles.card_imagen} />
        <div>
          <h1>{data.title}</h1>
          <p>{data.description}</p>
          <a target="_black">Ver más</a>
        </div>
      </a>
    </Link>
  );
}
