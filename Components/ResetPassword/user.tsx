import NavBar from "../NavBar";
import styles from "./index.module.css";
import Footer from "../Footer";
import { Form, Input, Button } from "antd";
import axios from "axios";
import { MAIN_URL } from "../../utils/Urls";
import { useRouter } from "next/router";
import { useState } from "react";

export default function ResetUser(props: any) {
  const [Loading, setLoading] = useState(false);
  const { token, email } = props;
  const router = useRouter();

  return (
    <div>
      <NavBar home={false} />
      <div className={styles.forgot_content}>
        <div className={styles.body_cont}>
          <div className={styles.body_box}>
            <h3>Recuperar contraseña</h3>
            <Form
              name="normal_login"
              className={styles.login_form}
              initialValues={{ remember: true }}
              onFinish={(values) => {
                const handleSubmit = () => {
                  setLoading(true);
                  const url = `${MAIN_URL}/resetPassword`;
                  axios
                    .post(url, {
                      password: values.password,
                      email: email,
                      token: token,
                    })
                    .then((res) => {
                      setLoading(false);
                      router.push("/login");
                    })
                    .catch((err) => {
                      setLoading(false);
                      console.log("error:", err);
                    });
                };
                handleSubmit();
              }}
            >
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Por favor ingrese una contraseña!",
                  },
                ]}
                hasFeedback
              >
                <Input.Password
                  className={styles.form_control}
                  placeholder="Contraseña"
                />
              </Form.Item>

              <Form.Item
                name="confirm"
                dependencies={["password"]}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Por favor consfirme su contraseña!",
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }

                      return Promise.reject(
                        "Las dos contraseñas que ingresaste no coinciden!"
                      );
                    },
                  }),
                ]}
              >
                <Input.Password
                  className={styles.form_control}
                  placeholder="Contraseña"
                />
              </Form.Item>

              <Form.Item>
                <Button
                  loading={Loading}
                  style={{ width: "100%" }}
                  type="primary"
                  htmlType="submit"
                  className="btn-btn-primary"
                >
                  Enviar
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
}
