import { newQuery } from "./query";
import { newMutation } from "./mutations";

export { newQuery, newMutation };
