import gql from "graphql-tag";

const GET_USER = gql`
  query getUsuario {
    getUsuario {
      message
      success
      data {
        _id
        name
        lastName
        email
        city
        avatar
        phone
        created_at
        updated_at
        termAndConditions
        isAvalible
        nie
        adress
        isAdmin
        createdBy
        iban
      }
    }
  }
`;

const GET_USER_ALL = gql`
  query getUserAdmin($search: String, $page: Int, $limit: Int) {
    getUserAdmin(search: $search, page: $page, limit: $limit) {
      message
      success
      status
      count
      data {
        _id
        name
        lastName
        email
        city
        avatar
        phone
        created_at
        updated_at
        termAndConditions
        isAvalible
        nie
        adress
        isAdmin
        createdBy
        iban
      }
    }
  }
`;

const GET_COMUNICADO = gql`
  query getComunicados($type: String) {
    getComunicados(type: $type) {
      message
      success
      data {
        _id
        title
        type
        content
        created_at
        user
      }
    }
  }
`;

const GET_POST = gql`
  query getPost {
    getPost {
      message
      success
      data {
        _id
        title
        type
        event
        revista
        image
        description
        created_at
        link
        user
      }
    }
  }
`;

const GET_COMUNICADO_ID = gql`
  query getComunicadosID($id: ID) {
    getComunicadosID(id: $id) {
      message
      success
      data {
        _id
        title
        type
        content
        created_at
        user
      }
    }
  }
`;

const GET_POST_PARAM = gql`
  query getPostParam($input: JSON, $page: Int, $limit: Int) {
    getPostParam(input: $input, page: $page, limit: $limit) {
      message
      success
      data {
        _id
        title
        type
        event
        revista
        image
        description
        created_at
        link
        user
      }
    }
  }
`;

const GET_RANDOM_ADS = gql`
  query getAds {
    getAds {
      messages
      success
      data {
        _id
        name
        image
        sorting
        visible
        navigate
        includeCity
        url
        email
        category
        isHome
        dataEmail
        click
        created_at
        end_date
      }
    }
  }
`;

const GET_PROVEEDOR = gql`
  query getProveedor($search: String, $page: Int, $limit: Int) {
    getProveedor(search: $search, page: $page, limit: $limit) {
      success
      message
      status
      count
      data {
        _id
        name
        imagen
        description
        mark
        products
        phone
        city
        cede
        vendedores
        user
        url
        created_at
      }
    }
  }
`;

export const newQuery = {
  GET_USER,
  GET_COMUNICADO,
  GET_USER_ALL,
  GET_POST,
  GET_COMUNICADO_ID,
  GET_POST_PARAM,
  GET_RANDOM_ADS,
  GET_PROVEEDOR,
};
