import gql from "graphql-tag";

export const AUTENTICAR_USUARIO = gql`
  mutation LoginUser($email: String!, $password: String!) {
    LoginUser(email: $email, password: $password) {
      success
      message
      data {
        token
        id
      }
    }
  }
`;

const ACTUALIZAR_USUARIO = gql`
  mutation actualizarUsuario($input: JSON) {
    actualizarUsuario(input: $input) {
      success
      message
      data {
        _id
        name
        lastName
        email
        city
        avatar
        phone
        created_at
        updated_at
        termAndConditions
        isAvalible
        nie
        adress
        isAdmin
        createdBy
      }
    }
  }
`;

const CREAR_USUARIO = gql`
  mutation crearUsuario($input: JSON) {
    crearUsuario(input: $input) {
      success
      message
      data {
        _id
        name
        lastName
        email
        city
        avatar
        phone
        created_at
        updated_at
        termAndConditions
        isAvalible
        nie
        adress
        isAdmin
        createdBy
      }
    }
  }
`;

const CREAR_COMUNICADO = gql`
  mutation crearCommunicado($input: JSON) {
    crearCommunicado(input: $input) {
      success
      message
      data {
        _id
        title
        type
        content
        created_at
        user
      }
    }
  }
`;

const ELIMINAR_COMUNICADO = gql`
  mutation eliminarComunication($id: ID) {
    eliminarComunication(id: $id) {
      success
      messages
    }
  }
`;

const DELETE_USER = gql`
  mutation eliminarUsuario($id: ID) {
    eliminarUsuario(id: $id) {
      success
      messages
    }
  }
`;

const DELETE_POST = gql`
  mutation eliminarPost($id: ID) {
    eliminarPost(id: $id) {
      success
      messages
    }
  }
`;

const CREAR_POST = gql`
  mutation crearPost($input: JSON) {
    crearPost(input: $input) {
      success
      message
      data {
        _id
        title
        type
        image
        description
        created_at
        link
        user
      }
    }
  }
`;

const CREATED_ADS = gql`
  mutation createAds($data: AdsData) {
    createAds(data: $data) {
      messages
      success
    }
  }
`;

const UPDATE_ADS = gql`
  mutation updateAds($data: JSON) {
    updateAds(data: $data) {
      messages
      success
    }
  }
`;

const DELETED_ADS = gql`
  mutation eliminarAds($id: String) {
    eliminarAds(id: $id) {
      messages
      success
    }
  }
`;

const UPLOAD_FILE_STORE = gql`
  mutation singleUploadToStoreImagenAws($file: Upload) {
    singleUploadToStoreImagenAws(file: $file) {
      data
    }
  }
`;

const UPDATE_ADS_CLICK = gql`
  mutation updateAds($data: JSON) {
    updateAds(data: $data) {
      success
      messages
    }
  }
`;

const CREATED_PROVEEDOR = gql`
  mutation createProveedor($input: JSON) {
    createProveedor(input: $input) {
      success
      messages
    }
  }
`;

const UPDATE_PROVEEDOR = gql`
  mutation updateProveedor($input: JSON) {
    updateProveedor(input: $input) {
      success
      messages
    }
  }
`;

const DELETE_PROVEEDOR = gql`
  mutation eliminarProveedor($id: ID) {
    eliminarProveedor(id: $id) {
      success
      messages
    }
  }
`;

export const newMutation = {
  AUTENTICAR_USUARIO,
  ACTUALIZAR_USUARIO,
  CREAR_USUARIO,
  CREAR_COMUNICADO,
  ELIMINAR_COMUNICADO,
  DELETE_USER,
  DELETE_POST,
  CREAR_POST,
  CREATED_ADS,
  DELETED_ADS,
  UPDATE_ADS,
  UPLOAD_FILE_STORE,
  UPDATE_ADS_CLICK,
  CREATED_PROVEEDOR,
  UPDATE_PROVEEDOR,
  DELETE_PROVEEDOR,
};
