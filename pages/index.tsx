import type { NextPage } from "next";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import HomeComponent from "../Components/Home";
const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Federación de Hostelería de Burgos</title>
        <meta name="description" content="Federación de Hostelería de Burgos" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <HomeComponent />
    </div>
  );
};

export default Home;
