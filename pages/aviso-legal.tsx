import React from "react";
import Condiiton from "../Components/helpPage/avisoLegal";
import Head from "next/head";

export default function Abouts() {
  return (
    <div>
      <Head>
        <title>Aviso legal | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Aviso legal | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Condiiton />
    </div>
  );
}
