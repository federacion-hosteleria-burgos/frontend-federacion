import React from "react";
import Turismo from "../Components/Turismo";
import Head from "next/head";

export default function Noticias() {
  return (
    <div>
      <Head>
        <title>
          Información turistica | Federación de Hostelería de Burgos
        </title>
        <meta
          name="description"
          content="Información turistica | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Turismo />
    </div>
  );
}
