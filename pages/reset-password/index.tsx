import { useRouter } from "next/router";
import ResetUser from "../../Components/ResetPassword/user";
import Head from "next/head";

export default function ForgotPassword() {
  const router = useRouter();
  const { token, email } = router.query;
  return (
    <div>
      <Head>
        <title>
          Recupera tu contraseña | Federación de Hostelería de Burgos
        </title>
        <meta
          name="description"
          content="Recupera tu contraseña | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ResetUser token={token} email={email} />
    </div>
  );
}
