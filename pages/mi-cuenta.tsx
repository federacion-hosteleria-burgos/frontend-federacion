import React from "react";
import Head from "next/head";
import Profile from "../Components/Profile";

export default function MiCuenta() {
  return (
    <div>
      <Head>
        <title>Mi cuenta | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Mi cuenta | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Profile />
    </div>
  );
}
