import React from "react";
import Noticia from "../Components/Feed";
import Head from "next/head";

export default function Noticias() {
  return (
    <div>
      <Head>
        <title>Noticias | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Noticias | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Noticia />
    </div>
  );
}
