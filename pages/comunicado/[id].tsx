import React from "react";
import { useRouter } from "next/router";
import Detalles from "../../Components/Profile/Comunicados/Detalle";

export default function Comunicado() {
  const router = useRouter();
  const { id } = router.query;
  return (
    <div>
      <Detalles id={id} />
    </div>
  );
}
