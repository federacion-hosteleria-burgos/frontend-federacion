import React from "react";
import LoginComponents from "../Components/Login";
import Head from "next/head";

export default function Login() {
  return (
    <div>
      <Head>
        <title>
          Inicia sesión en tu cuenta | Federación de Hostelería de Burgos
        </title>
        <meta
          name="description"
          content="Inicia sesión en tu cuenta | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <LoginComponents />
    </div>
  );
}
