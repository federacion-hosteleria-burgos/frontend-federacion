import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    const APP_NAME = "Federación de Hosteleria Burgos";
    const APP_DESCRIPTION = "Federación de Hosteleria Burgos";

    return (
      <Html lang="es">
        <Head>
          <meta name="application-name" content={APP_NAME} />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="default"
          />
          <meta name="apple-mobile-web-app-title" content={APP_NAME} />
          <meta name="description" content={APP_DESCRIPTION} />
          <meta name="format-detection" content="telephone=no" />
          <meta name="mobile-web-app-capable" content="yes" />
          <meta name="theme-color" content="#FFFFFF" />

          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/icons/apple-touch-icon.png"
          />
          <link rel="manifest" href="/manifest.json" />
          <link rel="shortcut icon" href="/favicon.ico" />

          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;700;900&display=swap"
            rel="stylesheet"
          />
          <script
            type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA&libraries=places"
          />

            <link rel="stylesheet" href="https://pdcc.gdpr.es/pdcc.min.css" />
            <script charset="utf-8" src="https://pdcc.gdpr.es/pdcc.min.js"></script>

            <script
          id="tiktok-bussiness-analytics"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
            PDCookieConsent.config({
              "brand": {
              "dev" : false,
              "name": "FEDERACION HOSTELERIA BURGOS",
              "url" : "https://federacionhosteleriaburgos.es/politica-de-cookies",
              "websiteOwner" : "https://federacionhosteleriaburgos.es"
              },
              "cookiePolicyLink": "https://federacionhosteleriaburgos.es/politica-de-cookies",
              "hideModalIn": [""],
              "styles": {
              "primaryButton": {
              "bgColor" : "#b37b02",
              "txtColor": "#fff"
              },
              "secondaryButton": {
              "bgColor" : "#EEEEEE",
              "txtColor": "#333333"
              }
              }
              });
            `,
          }}
        />

        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
