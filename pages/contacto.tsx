import React from "react";
import Contacto from "../Components/Contact";
import Head from "next/head";

export default function Contact() {
  return (
    <div>
      <Head>
        <title>Contacto | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Contacto | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Contacto />
    </div>
  );
}
