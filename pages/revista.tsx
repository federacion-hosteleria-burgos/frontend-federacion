import React from "react";
import Revistas from "../Components/Revistas";
import Head from "next/head";

export default function Noticias() {
  return (
    <div>
      <Head>
        <title>Revistas | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Revistas | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Revistas />
    </div>
  );
}
