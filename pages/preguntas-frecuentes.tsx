import React from "react";
import QYA from "../Components/helpPage/QyA";
import Head from "next/head";

export default function Abouts() {
  return (
    <div>
      <Head>
        <title>Preguntas frecuentes | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Preguntas frecuentes | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <QYA />
    </div>
  );
}
