import React from "react";
import About from "../Components/About";
import Head from "next/head";

export default function Abouts() {
  return (
    <div>
      <Head>
        <title>Sobre nosotros | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Sobre nosotros | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <About />
    </div>
  );
}
