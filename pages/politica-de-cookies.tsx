import React from "react";
import Cookies from "../Components/helpPage/cookies";
import Head from "next/head";

export default function Abouts() {
  return (
    <div>
      <Head>
        <title>Políticas de Cookies | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Políticas de Cookies | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Cookies />
    </div>
  );
}
