import "../styles/globals.css";
import "../styles/antd.css";
import "../styles/index.css";
import "../styles/quill.snow.css";
import type { AppProps } from "next/app";
import { Shield } from "../NextShield/Shield";
import { ApolloProvider } from "@apollo/client";
import client from "../apollo-client";
import "moment/locale/es";

function MyApp({ Component, pageProps }: AppProps) {
  if (typeof window === "undefined") {
    return <></>;
  } else {
    return (
      <Shield>
        <ApolloProvider client={client}>
          <Component {...pageProps} />
        </ApolloProvider>
      </Shield>
    );
  }
}
export default MyApp;
