import React from "react";
import Privacity from "../Components/helpPage/privacity";
import Head from "next/head";

export default function Abouts() {
  return (
    <div>
      <Head>
        <title>Políticas de privacidad | Federación de Hostelería de Burgos</title>
        <meta
          name="description"
          content="Políticas de privacidad | Federación de Hostelería de Burgos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Privacity />
    </div>
  );
}
